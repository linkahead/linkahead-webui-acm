/**
 * @fileoverview gRPC-Web generated client stub for caosdb.acm.v1alpha1
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.caosdb = {};
proto.caosdb.acm = {};
proto.caosdb.acm.v1alpha1 = require('./main_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.ListUsersRequest,
 *   !proto.caosdb.acm.v1alpha1.ListUsersResponse>}
 */
const methodDescriptor_AccessControlManagementService_ListUsers = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/ListUsers',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.ListUsersRequest,
  proto.caosdb.acm.v1alpha1.ListUsersResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.ListUsersRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.ListUsersResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.ListUsersRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.ListUsersResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.ListUsersResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.listUsers =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/ListUsers',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_ListUsers,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.ListUsersRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.ListUsersResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.listUsers =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/ListUsers',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_ListUsers);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.RetrieveSingleUserRequest,
 *   !proto.caosdb.acm.v1alpha1.RetrieveSingleUserResponse>}
 */
const methodDescriptor_AccessControlManagementService_RetrieveSingleUser = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/RetrieveSingleUser',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.RetrieveSingleUserRequest,
  proto.caosdb.acm.v1alpha1.RetrieveSingleUserResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.RetrieveSingleUserRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.RetrieveSingleUserResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.RetrieveSingleUserRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.RetrieveSingleUserResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.RetrieveSingleUserResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.retrieveSingleUser =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/RetrieveSingleUser',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_RetrieveSingleUser,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.RetrieveSingleUserRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.RetrieveSingleUserResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.retrieveSingleUser =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/RetrieveSingleUser',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_RetrieveSingleUser);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.CreateSingleUserRequest,
 *   !proto.caosdb.acm.v1alpha1.CreateSingleUserResponse>}
 */
const methodDescriptor_AccessControlManagementService_CreateSingleUser = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/CreateSingleUser',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.CreateSingleUserRequest,
  proto.caosdb.acm.v1alpha1.CreateSingleUserResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.CreateSingleUserRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.CreateSingleUserResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.CreateSingleUserRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.CreateSingleUserResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.CreateSingleUserResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.createSingleUser =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/CreateSingleUser',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_CreateSingleUser,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.CreateSingleUserRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.CreateSingleUserResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.createSingleUser =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/CreateSingleUser',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_CreateSingleUser);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.UpdateSingleUserRequest,
 *   !proto.caosdb.acm.v1alpha1.UpdateSingleUserResponse>}
 */
const methodDescriptor_AccessControlManagementService_UpdateSingleUser = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/UpdateSingleUser',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.UpdateSingleUserRequest,
  proto.caosdb.acm.v1alpha1.UpdateSingleUserResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.UpdateSingleUserRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.UpdateSingleUserResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.UpdateSingleUserRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.UpdateSingleUserResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.UpdateSingleUserResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.updateSingleUser =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/UpdateSingleUser',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_UpdateSingleUser,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.UpdateSingleUserRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.UpdateSingleUserResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.updateSingleUser =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/UpdateSingleUser',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_UpdateSingleUser);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.DeleteSingleUserRequest,
 *   !proto.caosdb.acm.v1alpha1.DeleteSingleUserResponse>}
 */
const methodDescriptor_AccessControlManagementService_DeleteSingleUser = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/DeleteSingleUser',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.DeleteSingleUserRequest,
  proto.caosdb.acm.v1alpha1.DeleteSingleUserResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.DeleteSingleUserRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.DeleteSingleUserResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.DeleteSingleUserRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.DeleteSingleUserResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.DeleteSingleUserResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.deleteSingleUser =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/DeleteSingleUser',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_DeleteSingleUser,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.DeleteSingleUserRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.DeleteSingleUserResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.deleteSingleUser =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/DeleteSingleUser',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_DeleteSingleUser);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.ListRolesRequest,
 *   !proto.caosdb.acm.v1alpha1.ListRolesResponse>}
 */
const methodDescriptor_AccessControlManagementService_ListRoles = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/ListRoles',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.ListRolesRequest,
  proto.caosdb.acm.v1alpha1.ListRolesResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.ListRolesRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.ListRolesResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.ListRolesRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.ListRolesResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.ListRolesResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.listRoles =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/ListRoles',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_ListRoles,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.ListRolesRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.ListRolesResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.listRoles =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/ListRoles',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_ListRoles);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.RetrieveSingleRoleRequest,
 *   !proto.caosdb.acm.v1alpha1.RetrieveSingleRoleResponse>}
 */
const methodDescriptor_AccessControlManagementService_RetrieveSingleRole = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/RetrieveSingleRole',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.RetrieveSingleRoleRequest,
  proto.caosdb.acm.v1alpha1.RetrieveSingleRoleResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.RetrieveSingleRoleRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.RetrieveSingleRoleResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.RetrieveSingleRoleRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.RetrieveSingleRoleResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.RetrieveSingleRoleResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.retrieveSingleRole =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/RetrieveSingleRole',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_RetrieveSingleRole,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.RetrieveSingleRoleRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.RetrieveSingleRoleResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.retrieveSingleRole =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/RetrieveSingleRole',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_RetrieveSingleRole);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.CreateSingleRoleRequest,
 *   !proto.caosdb.acm.v1alpha1.CreateSingleRoleResponse>}
 */
const methodDescriptor_AccessControlManagementService_CreateSingleRole = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/CreateSingleRole',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.CreateSingleRoleRequest,
  proto.caosdb.acm.v1alpha1.CreateSingleRoleResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.CreateSingleRoleRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.CreateSingleRoleResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.CreateSingleRoleRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.CreateSingleRoleResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.CreateSingleRoleResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.createSingleRole =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/CreateSingleRole',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_CreateSingleRole,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.CreateSingleRoleRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.CreateSingleRoleResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.createSingleRole =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/CreateSingleRole',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_CreateSingleRole);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.UpdateSingleRoleRequest,
 *   !proto.caosdb.acm.v1alpha1.UpdateSingleRoleResponse>}
 */
const methodDescriptor_AccessControlManagementService_UpdateSingleRole = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/UpdateSingleRole',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.UpdateSingleRoleRequest,
  proto.caosdb.acm.v1alpha1.UpdateSingleRoleResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.UpdateSingleRoleRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.UpdateSingleRoleResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.UpdateSingleRoleRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.UpdateSingleRoleResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.UpdateSingleRoleResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.updateSingleRole =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/UpdateSingleRole',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_UpdateSingleRole,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.UpdateSingleRoleRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.UpdateSingleRoleResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.updateSingleRole =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/UpdateSingleRole',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_UpdateSingleRole);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.DeleteSingleRoleRequest,
 *   !proto.caosdb.acm.v1alpha1.DeleteSingleRoleResponse>}
 */
const methodDescriptor_AccessControlManagementService_DeleteSingleRole = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/DeleteSingleRole',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.DeleteSingleRoleRequest,
  proto.caosdb.acm.v1alpha1.DeleteSingleRoleResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.DeleteSingleRoleRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.DeleteSingleRoleResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.DeleteSingleRoleRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.DeleteSingleRoleResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.DeleteSingleRoleResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.deleteSingleRole =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/DeleteSingleRole',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_DeleteSingleRole,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.DeleteSingleRoleRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.DeleteSingleRoleResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.deleteSingleRole =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/DeleteSingleRole',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_DeleteSingleRole);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.acm.v1alpha1.ListKnownPermissionsRequest,
 *   !proto.caosdb.acm.v1alpha1.ListKnownPermissionsResponse>}
 */
const methodDescriptor_AccessControlManagementService_ListKnownPermissions = new grpc.web.MethodDescriptor(
  '/caosdb.acm.v1alpha1.AccessControlManagementService/ListKnownPermissions',
  grpc.web.MethodType.UNARY,
  proto.caosdb.acm.v1alpha1.ListKnownPermissionsRequest,
  proto.caosdb.acm.v1alpha1.ListKnownPermissionsResponse,
  /**
   * @param {!proto.caosdb.acm.v1alpha1.ListKnownPermissionsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.acm.v1alpha1.ListKnownPermissionsResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.acm.v1alpha1.ListKnownPermissionsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.acm.v1alpha1.ListKnownPermissionsResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.acm.v1alpha1.ListKnownPermissionsResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServiceClient.prototype.listKnownPermissions =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/ListKnownPermissions',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_ListKnownPermissions,
      callback);
};


/**
 * @param {!proto.caosdb.acm.v1alpha1.ListKnownPermissionsRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.acm.v1alpha1.ListKnownPermissionsResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.acm.v1alpha1.AccessControlManagementServicePromiseClient.prototype.listKnownPermissions =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.acm.v1alpha1.AccessControlManagementService/ListKnownPermissions',
      request,
      metadata || {},
      methodDescriptor_AccessControlManagementService_ListKnownPermissions);
};


module.exports = proto.caosdb.acm.v1alpha1;

