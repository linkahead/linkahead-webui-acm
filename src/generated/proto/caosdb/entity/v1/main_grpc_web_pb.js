/**
 * @fileoverview gRPC-Web generated client stub for caosdb.entity.v1
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.caosdb = {};
proto.caosdb.entity = {};
proto.caosdb.entity.v1 = require('./main_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.caosdb.entity.v1.EntityTransactionServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.caosdb.entity.v1.EntityTransactionServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.entity.v1.MultiTransactionRequest,
 *   !proto.caosdb.entity.v1.MultiTransactionResponse>}
 */
const methodDescriptor_EntityTransactionService_MultiTransaction = new grpc.web.MethodDescriptor(
  '/caosdb.entity.v1.EntityTransactionService/MultiTransaction',
  grpc.web.MethodType.UNARY,
  proto.caosdb.entity.v1.MultiTransactionRequest,
  proto.caosdb.entity.v1.MultiTransactionResponse,
  /**
   * @param {!proto.caosdb.entity.v1.MultiTransactionRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.entity.v1.MultiTransactionResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.entity.v1.MultiTransactionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.entity.v1.MultiTransactionResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.entity.v1.MultiTransactionResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.entity.v1.EntityTransactionServiceClient.prototype.multiTransaction =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.entity.v1.EntityTransactionService/MultiTransaction',
      request,
      metadata || {},
      methodDescriptor_EntityTransactionService_MultiTransaction,
      callback);
};


/**
 * @param {!proto.caosdb.entity.v1.MultiTransactionRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.entity.v1.MultiTransactionResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.entity.v1.EntityTransactionServicePromiseClient.prototype.multiTransaction =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.entity.v1.EntityTransactionService/MultiTransaction',
      request,
      metadata || {},
      methodDescriptor_EntityTransactionService_MultiTransaction);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest,
 *   !proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse>}
 */
const methodDescriptor_EntityTransactionService_MultiRetrieveEntityACL = new grpc.web.MethodDescriptor(
  '/caosdb.entity.v1.EntityTransactionService/MultiRetrieveEntityACL',
  grpc.web.MethodType.UNARY,
  proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest,
  proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse,
  /**
   * @param {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.entity.v1.EntityTransactionServiceClient.prototype.multiRetrieveEntityACL =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.entity.v1.EntityTransactionService/MultiRetrieveEntityACL',
      request,
      metadata || {},
      methodDescriptor_EntityTransactionService_MultiRetrieveEntityACL,
      callback);
};


/**
 * @param {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.entity.v1.EntityTransactionServicePromiseClient.prototype.multiRetrieveEntityACL =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.entity.v1.EntityTransactionService/MultiRetrieveEntityACL',
      request,
      metadata || {},
      methodDescriptor_EntityTransactionService_MultiRetrieveEntityACL);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.entity.v1.MultiUpdateEntityACLRequest,
 *   !proto.caosdb.entity.v1.MultiUpdateEntityACLResponse>}
 */
const methodDescriptor_EntityTransactionService_MultiUpdateEntityACL = new grpc.web.MethodDescriptor(
  '/caosdb.entity.v1.EntityTransactionService/MultiUpdateEntityACL',
  grpc.web.MethodType.UNARY,
  proto.caosdb.entity.v1.MultiUpdateEntityACLRequest,
  proto.caosdb.entity.v1.MultiUpdateEntityACLResponse,
  /**
   * @param {!proto.caosdb.entity.v1.MultiUpdateEntityACLRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.entity.v1.MultiUpdateEntityACLRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.entity.v1.MultiUpdateEntityACLResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.entity.v1.MultiUpdateEntityACLResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.entity.v1.EntityTransactionServiceClient.prototype.multiUpdateEntityACL =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.entity.v1.EntityTransactionService/MultiUpdateEntityACL',
      request,
      metadata || {},
      methodDescriptor_EntityTransactionService_MultiUpdateEntityACL,
      callback);
};


/**
 * @param {!proto.caosdb.entity.v1.MultiUpdateEntityACLRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.entity.v1.MultiUpdateEntityACLResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.entity.v1.EntityTransactionServicePromiseClient.prototype.multiUpdateEntityACL =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.entity.v1.EntityTransactionService/MultiUpdateEntityACL',
      request,
      metadata || {},
      methodDescriptor_EntityTransactionService_MultiUpdateEntityACL);
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.caosdb.entity.v1.FileTransmissionServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.caosdb.entity.v1.FileTransmissionServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.entity.v1.RegisterFileUploadRequest,
 *   !proto.caosdb.entity.v1.RegisterFileUploadResponse>}
 */
const methodDescriptor_FileTransmissionService_RegisterFileUpload = new grpc.web.MethodDescriptor(
  '/caosdb.entity.v1.FileTransmissionService/RegisterFileUpload',
  grpc.web.MethodType.UNARY,
  proto.caosdb.entity.v1.RegisterFileUploadRequest,
  proto.caosdb.entity.v1.RegisterFileUploadResponse,
  /**
   * @param {!proto.caosdb.entity.v1.RegisterFileUploadRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.entity.v1.RegisterFileUploadResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.entity.v1.RegisterFileUploadRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.caosdb.entity.v1.RegisterFileUploadResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.entity.v1.RegisterFileUploadResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.caosdb.entity.v1.FileTransmissionServiceClient.prototype.registerFileUpload =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/caosdb.entity.v1.FileTransmissionService/RegisterFileUpload',
      request,
      metadata || {},
      methodDescriptor_FileTransmissionService_RegisterFileUpload,
      callback);
};


/**
 * @param {!proto.caosdb.entity.v1.RegisterFileUploadRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.caosdb.entity.v1.RegisterFileUploadResponse>}
 *     Promise that resolves to the response
 */
proto.caosdb.entity.v1.FileTransmissionServicePromiseClient.prototype.registerFileUpload =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/caosdb.entity.v1.FileTransmissionService/RegisterFileUpload',
      request,
      metadata || {},
      methodDescriptor_FileTransmissionService_RegisterFileUpload);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.caosdb.entity.v1.FileDownloadRequest,
 *   !proto.caosdb.entity.v1.FileDownloadResponse>}
 */
const methodDescriptor_FileTransmissionService_FileDownload = new grpc.web.MethodDescriptor(
  '/caosdb.entity.v1.FileTransmissionService/FileDownload',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.caosdb.entity.v1.FileDownloadRequest,
  proto.caosdb.entity.v1.FileDownloadResponse,
  /**
   * @param {!proto.caosdb.entity.v1.FileDownloadRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.caosdb.entity.v1.FileDownloadResponse.deserializeBinary
);


/**
 * @param {!proto.caosdb.entity.v1.FileDownloadRequest} request The request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.entity.v1.FileDownloadResponse>}
 *     The XHR Node Readable Stream
 */
proto.caosdb.entity.v1.FileTransmissionServiceClient.prototype.fileDownload =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/caosdb.entity.v1.FileTransmissionService/FileDownload',
      request,
      metadata || {},
      methodDescriptor_FileTransmissionService_FileDownload);
};


/**
 * @param {!proto.caosdb.entity.v1.FileDownloadRequest} request The request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.caosdb.entity.v1.FileDownloadResponse>}
 *     The XHR Node Readable Stream
 */
proto.caosdb.entity.v1.FileTransmissionServicePromiseClient.prototype.fileDownload =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/caosdb.entity.v1.FileTransmissionService/FileDownload',
      request,
      metadata || {},
      methodDescriptor_FileTransmissionService_FileDownload);
};


module.exports = proto.caosdb.entity.v1;

