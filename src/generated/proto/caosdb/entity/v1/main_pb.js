// source: caosdb/entity/v1/main.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.caosdb.entity.v1.AtomicDataType', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.CollectionValues', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.DataType', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.DataType.DataTypeCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.DeleteRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.DeleteRequest.WrappedRequestCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.DeleteResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.DeleteResponse.WrappedResponseCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.Entity', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.EntityACL', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.EntityPermission', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.EntityPermissionRule', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.EntityRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.EntityResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.EntityRole', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.FileChunk', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.FileDescriptor', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.FileDownloadRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.FileDownloadResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.FileTransmissionId', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.FileTransmissionSettings', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.FileUploadRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.FileUploadResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.FindQueryResult', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.Hash', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.IdResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.Importance', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.InsertRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.InsertResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.InsertResponse.WrappedResponseCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.ListDataType', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.ListDataType.ListDataTypeCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.Message', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.MessageCode', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.MultiTransactionRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.MultiTransactionResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.MultiUpdateEntityACLRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.MultiUpdateEntityACLResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.Parent', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.Property', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.Query', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.ReferenceDataType', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.RegisterFileUploadRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.RegisterFileUploadResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.RegistrationStatus', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.RetrieveRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.RetrieveRequest.WrappedRequestCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.RetrieveResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.RetrieveResponse.RetrieveResponseCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.ScalarValue', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.ScalarValue.ScalarValueCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.SelectQueryResult', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.SelectQueryRows', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.SpecialValue', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.TransactionRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.TransactionRequest.WrappedRequestsCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.TransactionResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.TransactionResponse.TransactionResponseCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.TransmissionStatus', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.UpdateRequest', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.UpdateResponse', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.UpdateResponse.WrappedResponseCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.Value', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.Value.ValueCase', null, global);
goog.exportSymbol('proto.caosdb.entity.v1.Version', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.ReferenceDataType = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.ReferenceDataType, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.ReferenceDataType.displayName = 'proto.caosdb.entity.v1.ReferenceDataType';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.ListDataType = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.ListDataType.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.ListDataType, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.ListDataType.displayName = 'proto.caosdb.entity.v1.ListDataType';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.DataType = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.DataType.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.DataType, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.DataType.displayName = 'proto.caosdb.entity.v1.DataType';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.CollectionValues = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.CollectionValues.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.CollectionValues, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.CollectionValues.displayName = 'proto.caosdb.entity.v1.CollectionValues';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.ScalarValue = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.ScalarValue.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.ScalarValue, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.ScalarValue.displayName = 'proto.caosdb.entity.v1.ScalarValue';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.Value = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.Value.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.Value, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.Value.displayName = 'proto.caosdb.entity.v1.Value';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.Message = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.Message, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.Message.displayName = 'proto.caosdb.entity.v1.Message';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.Version = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.Version, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.Version.displayName = 'proto.caosdb.entity.v1.Version';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.Entity = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.Entity.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.Entity, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.Entity.displayName = 'proto.caosdb.entity.v1.Entity';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.FileDescriptor = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.FileDescriptor.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.FileDescriptor, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.FileDescriptor.displayName = 'proto.caosdb.entity.v1.FileDescriptor';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.Hash = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.Hash, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.Hash.displayName = 'proto.caosdb.entity.v1.Hash';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.Property = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.Property.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.Property, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.Property.displayName = 'proto.caosdb.entity.v1.Property';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.Parent = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.Parent.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.Parent, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.Parent.displayName = 'proto.caosdb.entity.v1.Parent';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.EntityRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.EntityRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.EntityRequest.displayName = 'proto.caosdb.entity.v1.EntityRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.EntityResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.EntityResponse.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.EntityResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.EntityResponse.displayName = 'proto.caosdb.entity.v1.EntityResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.IdResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.IdResponse.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.IdResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.IdResponse.displayName = 'proto.caosdb.entity.v1.IdResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.Query = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.Query, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.Query.displayName = 'proto.caosdb.entity.v1.Query';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.FindQueryResult = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.FindQueryResult.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.FindQueryResult, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.FindQueryResult.displayName = 'proto.caosdb.entity.v1.FindQueryResult';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.SelectQueryRows = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.SelectQueryRows.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.SelectQueryRows, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.SelectQueryRows.displayName = 'proto.caosdb.entity.v1.SelectQueryRows';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.SelectQueryResult = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.SelectQueryResult.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.SelectQueryResult, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.SelectQueryResult.displayName = 'proto.caosdb.entity.v1.SelectQueryResult';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.RetrieveRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.RetrieveRequest.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.RetrieveRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.RetrieveRequest.displayName = 'proto.caosdb.entity.v1.RetrieveRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.RetrieveResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.RetrieveResponse.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.RetrieveResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.RetrieveResponse.displayName = 'proto.caosdb.entity.v1.RetrieveResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.DeleteRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.DeleteRequest.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.DeleteRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.DeleteRequest.displayName = 'proto.caosdb.entity.v1.DeleteRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.DeleteResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.DeleteResponse.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.DeleteResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.DeleteResponse.displayName = 'proto.caosdb.entity.v1.DeleteResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.UpdateRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.UpdateRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.UpdateRequest.displayName = 'proto.caosdb.entity.v1.UpdateRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.UpdateResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.UpdateResponse.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.UpdateResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.UpdateResponse.displayName = 'proto.caosdb.entity.v1.UpdateResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.InsertRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.InsertRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.InsertRequest.displayName = 'proto.caosdb.entity.v1.InsertRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.InsertResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.InsertResponse.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.InsertResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.InsertResponse.displayName = 'proto.caosdb.entity.v1.InsertResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.TransactionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.TransactionRequest.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.TransactionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.TransactionRequest.displayName = 'proto.caosdb.entity.v1.TransactionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.TransactionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, proto.caosdb.entity.v1.TransactionResponse.oneofGroups_);
};
goog.inherits(proto.caosdb.entity.v1.TransactionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.TransactionResponse.displayName = 'proto.caosdb.entity.v1.TransactionResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.MultiTransactionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.MultiTransactionRequest.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.MultiTransactionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.MultiTransactionRequest.displayName = 'proto.caosdb.entity.v1.MultiTransactionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.MultiTransactionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.MultiTransactionResponse.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.MultiTransactionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.MultiTransactionResponse.displayName = 'proto.caosdb.entity.v1.MultiTransactionResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.displayName = 'proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.displayName = 'proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.MultiUpdateEntityACLRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.displayName = 'proto.caosdb.entity.v1.MultiUpdateEntityACLRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.MultiUpdateEntityACLResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.displayName = 'proto.caosdb.entity.v1.MultiUpdateEntityACLResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.EntityACL = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.EntityACL.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.EntityACL, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.EntityACL.displayName = 'proto.caosdb.entity.v1.EntityACL';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.EntityPermissionRule = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.caosdb.entity.v1.EntityPermissionRule.repeatedFields_, null);
};
goog.inherits(proto.caosdb.entity.v1.EntityPermissionRule, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.EntityPermissionRule.displayName = 'proto.caosdb.entity.v1.EntityPermissionRule';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.EntityPermission = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.EntityPermission, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.EntityPermission.displayName = 'proto.caosdb.entity.v1.EntityPermission';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.FileChunk = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.FileChunk, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.FileChunk.displayName = 'proto.caosdb.entity.v1.FileChunk';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.FileTransmissionId = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.FileTransmissionId, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.FileTransmissionId.displayName = 'proto.caosdb.entity.v1.FileTransmissionId';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.FileTransmissionSettings = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.FileTransmissionSettings, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.FileTransmissionSettings.displayName = 'proto.caosdb.entity.v1.FileTransmissionSettings';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.RegisterFileUploadRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.RegisterFileUploadRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.RegisterFileUploadRequest.displayName = 'proto.caosdb.entity.v1.RegisterFileUploadRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.RegisterFileUploadResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.RegisterFileUploadResponse.displayName = 'proto.caosdb.entity.v1.RegisterFileUploadResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.FileUploadRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.FileUploadRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.FileUploadRequest.displayName = 'proto.caosdb.entity.v1.FileUploadRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.FileUploadResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.FileUploadResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.FileUploadResponse.displayName = 'proto.caosdb.entity.v1.FileUploadResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.FileDownloadRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.FileDownloadRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.FileDownloadRequest.displayName = 'proto.caosdb.entity.v1.FileDownloadRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.caosdb.entity.v1.FileDownloadResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.caosdb.entity.v1.FileDownloadResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.caosdb.entity.v1.FileDownloadResponse.displayName = 'proto.caosdb.entity.v1.FileDownloadResponse';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.ReferenceDataType.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.ReferenceDataType.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.ReferenceDataType} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.ReferenceDataType.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.ReferenceDataType}
 */
proto.caosdb.entity.v1.ReferenceDataType.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.ReferenceDataType;
  return proto.caosdb.entity.v1.ReferenceDataType.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.ReferenceDataType} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.ReferenceDataType}
 */
proto.caosdb.entity.v1.ReferenceDataType.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.ReferenceDataType.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.ReferenceDataType.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.ReferenceDataType} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.ReferenceDataType.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.ReferenceDataType.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.ReferenceDataType} returns this
 */
proto.caosdb.entity.v1.ReferenceDataType.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.ListDataType.oneofGroups_ = [[1,2]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.ListDataType.ListDataTypeCase = {
  LIST_DATA_TYPE_NOT_SET: 0,
  ATOMIC_DATA_TYPE: 1,
  REFERENCE_DATA_TYPE: 2
};

/**
 * @return {proto.caosdb.entity.v1.ListDataType.ListDataTypeCase}
 */
proto.caosdb.entity.v1.ListDataType.prototype.getListDataTypeCase = function() {
  return /** @type {proto.caosdb.entity.v1.ListDataType.ListDataTypeCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.ListDataType.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.ListDataType.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.ListDataType.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.ListDataType} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.ListDataType.toObject = function(includeInstance, msg) {
  var f, obj = {
    atomicDataType: jspb.Message.getFieldWithDefault(msg, 1, 0),
    referenceDataType: (f = msg.getReferenceDataType()) && proto.caosdb.entity.v1.ReferenceDataType.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.ListDataType}
 */
proto.caosdb.entity.v1.ListDataType.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.ListDataType;
  return proto.caosdb.entity.v1.ListDataType.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.ListDataType} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.ListDataType}
 */
proto.caosdb.entity.v1.ListDataType.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.caosdb.entity.v1.AtomicDataType} */ (reader.readEnum());
      msg.setAtomicDataType(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.ReferenceDataType;
      reader.readMessage(value,proto.caosdb.entity.v1.ReferenceDataType.deserializeBinaryFromReader);
      msg.setReferenceDataType(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.ListDataType.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.ListDataType.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.ListDataType} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.ListDataType.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {!proto.caosdb.entity.v1.AtomicDataType} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getReferenceDataType();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.caosdb.entity.v1.ReferenceDataType.serializeBinaryToWriter
    );
  }
};


/**
 * optional AtomicDataType atomic_data_type = 1;
 * @return {!proto.caosdb.entity.v1.AtomicDataType}
 */
proto.caosdb.entity.v1.ListDataType.prototype.getAtomicDataType = function() {
  return /** @type {!proto.caosdb.entity.v1.AtomicDataType} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.caosdb.entity.v1.AtomicDataType} value
 * @return {!proto.caosdb.entity.v1.ListDataType} returns this
 */
proto.caosdb.entity.v1.ListDataType.prototype.setAtomicDataType = function(value) {
  return jspb.Message.setOneofField(this, 1, proto.caosdb.entity.v1.ListDataType.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.caosdb.entity.v1.ListDataType} returns this
 */
proto.caosdb.entity.v1.ListDataType.prototype.clearAtomicDataType = function() {
  return jspb.Message.setOneofField(this, 1, proto.caosdb.entity.v1.ListDataType.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.ListDataType.prototype.hasAtomicDataType = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional ReferenceDataType reference_data_type = 2;
 * @return {?proto.caosdb.entity.v1.ReferenceDataType}
 */
proto.caosdb.entity.v1.ListDataType.prototype.getReferenceDataType = function() {
  return /** @type{?proto.caosdb.entity.v1.ReferenceDataType} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.ReferenceDataType, 2));
};


/**
 * @param {?proto.caosdb.entity.v1.ReferenceDataType|undefined} value
 * @return {!proto.caosdb.entity.v1.ListDataType} returns this
*/
proto.caosdb.entity.v1.ListDataType.prototype.setReferenceDataType = function(value) {
  return jspb.Message.setOneofWrapperField(this, 2, proto.caosdb.entity.v1.ListDataType.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.ListDataType} returns this
 */
proto.caosdb.entity.v1.ListDataType.prototype.clearReferenceDataType = function() {
  return this.setReferenceDataType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.ListDataType.prototype.hasReferenceDataType = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.DataType.oneofGroups_ = [[1,2,3]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.DataType.DataTypeCase = {
  DATA_TYPE_NOT_SET: 0,
  ATOMIC_DATA_TYPE: 1,
  LIST_DATA_TYPE: 2,
  REFERENCE_DATA_TYPE: 3
};

/**
 * @return {proto.caosdb.entity.v1.DataType.DataTypeCase}
 */
proto.caosdb.entity.v1.DataType.prototype.getDataTypeCase = function() {
  return /** @type {proto.caosdb.entity.v1.DataType.DataTypeCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.DataType.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.DataType.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.DataType.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.DataType} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.DataType.toObject = function(includeInstance, msg) {
  var f, obj = {
    atomicDataType: jspb.Message.getFieldWithDefault(msg, 1, 0),
    listDataType: (f = msg.getListDataType()) && proto.caosdb.entity.v1.ListDataType.toObject(includeInstance, f),
    referenceDataType: (f = msg.getReferenceDataType()) && proto.caosdb.entity.v1.ReferenceDataType.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.DataType}
 */
proto.caosdb.entity.v1.DataType.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.DataType;
  return proto.caosdb.entity.v1.DataType.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.DataType} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.DataType}
 */
proto.caosdb.entity.v1.DataType.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.caosdb.entity.v1.AtomicDataType} */ (reader.readEnum());
      msg.setAtomicDataType(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.ListDataType;
      reader.readMessage(value,proto.caosdb.entity.v1.ListDataType.deserializeBinaryFromReader);
      msg.setListDataType(value);
      break;
    case 3:
      var value = new proto.caosdb.entity.v1.ReferenceDataType;
      reader.readMessage(value,proto.caosdb.entity.v1.ReferenceDataType.deserializeBinaryFromReader);
      msg.setReferenceDataType(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.DataType.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.DataType.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.DataType} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.DataType.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {!proto.caosdb.entity.v1.AtomicDataType} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getListDataType();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.caosdb.entity.v1.ListDataType.serializeBinaryToWriter
    );
  }
  f = message.getReferenceDataType();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.caosdb.entity.v1.ReferenceDataType.serializeBinaryToWriter
    );
  }
};


/**
 * optional AtomicDataType atomic_data_type = 1;
 * @return {!proto.caosdb.entity.v1.AtomicDataType}
 */
proto.caosdb.entity.v1.DataType.prototype.getAtomicDataType = function() {
  return /** @type {!proto.caosdb.entity.v1.AtomicDataType} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.caosdb.entity.v1.AtomicDataType} value
 * @return {!proto.caosdb.entity.v1.DataType} returns this
 */
proto.caosdb.entity.v1.DataType.prototype.setAtomicDataType = function(value) {
  return jspb.Message.setOneofField(this, 1, proto.caosdb.entity.v1.DataType.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.caosdb.entity.v1.DataType} returns this
 */
proto.caosdb.entity.v1.DataType.prototype.clearAtomicDataType = function() {
  return jspb.Message.setOneofField(this, 1, proto.caosdb.entity.v1.DataType.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.DataType.prototype.hasAtomicDataType = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional ListDataType list_data_type = 2;
 * @return {?proto.caosdb.entity.v1.ListDataType}
 */
proto.caosdb.entity.v1.DataType.prototype.getListDataType = function() {
  return /** @type{?proto.caosdb.entity.v1.ListDataType} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.ListDataType, 2));
};


/**
 * @param {?proto.caosdb.entity.v1.ListDataType|undefined} value
 * @return {!proto.caosdb.entity.v1.DataType} returns this
*/
proto.caosdb.entity.v1.DataType.prototype.setListDataType = function(value) {
  return jspb.Message.setOneofWrapperField(this, 2, proto.caosdb.entity.v1.DataType.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.DataType} returns this
 */
proto.caosdb.entity.v1.DataType.prototype.clearListDataType = function() {
  return this.setListDataType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.DataType.prototype.hasListDataType = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional ReferenceDataType reference_data_type = 3;
 * @return {?proto.caosdb.entity.v1.ReferenceDataType}
 */
proto.caosdb.entity.v1.DataType.prototype.getReferenceDataType = function() {
  return /** @type{?proto.caosdb.entity.v1.ReferenceDataType} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.ReferenceDataType, 3));
};


/**
 * @param {?proto.caosdb.entity.v1.ReferenceDataType|undefined} value
 * @return {!proto.caosdb.entity.v1.DataType} returns this
*/
proto.caosdb.entity.v1.DataType.prototype.setReferenceDataType = function(value) {
  return jspb.Message.setOneofWrapperField(this, 3, proto.caosdb.entity.v1.DataType.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.DataType} returns this
 */
proto.caosdb.entity.v1.DataType.prototype.clearReferenceDataType = function() {
  return this.setReferenceDataType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.DataType.prototype.hasReferenceDataType = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.CollectionValues.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.CollectionValues.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.CollectionValues.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.CollectionValues} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.CollectionValues.toObject = function(includeInstance, msg) {
  var f, obj = {
    valuesList: jspb.Message.toObjectList(msg.getValuesList(),
    proto.caosdb.entity.v1.ScalarValue.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.CollectionValues}
 */
proto.caosdb.entity.v1.CollectionValues.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.CollectionValues;
  return proto.caosdb.entity.v1.CollectionValues.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.CollectionValues} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.CollectionValues}
 */
proto.caosdb.entity.v1.CollectionValues.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.ScalarValue;
      reader.readMessage(value,proto.caosdb.entity.v1.ScalarValue.deserializeBinaryFromReader);
      msg.addValues(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.CollectionValues.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.CollectionValues.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.CollectionValues} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.CollectionValues.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValuesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.caosdb.entity.v1.ScalarValue.serializeBinaryToWriter
    );
  }
};


/**
 * repeated ScalarValue values = 1;
 * @return {!Array<!proto.caosdb.entity.v1.ScalarValue>}
 */
proto.caosdb.entity.v1.CollectionValues.prototype.getValuesList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.ScalarValue>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.ScalarValue, 1));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.ScalarValue>} value
 * @return {!proto.caosdb.entity.v1.CollectionValues} returns this
*/
proto.caosdb.entity.v1.CollectionValues.prototype.setValuesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.caosdb.entity.v1.ScalarValue=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.ScalarValue}
 */
proto.caosdb.entity.v1.CollectionValues.prototype.addValues = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.caosdb.entity.v1.ScalarValue, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.CollectionValues} returns this
 */
proto.caosdb.entity.v1.CollectionValues.prototype.clearValuesList = function() {
  return this.setValuesList([]);
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.ScalarValue.oneofGroups_ = [[1,2,3,4,5]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.ScalarValue.ScalarValueCase = {
  SCALAR_VALUE_NOT_SET: 0,
  INTEGER_VALUE: 1,
  DOUBLE_VALUE: 2,
  BOOLEAN_VALUE: 3,
  STRING_VALUE: 4,
  SPECIAL_VALUE: 5
};

/**
 * @return {proto.caosdb.entity.v1.ScalarValue.ScalarValueCase}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.getScalarValueCase = function() {
  return /** @type {proto.caosdb.entity.v1.ScalarValue.ScalarValueCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.ScalarValue.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.ScalarValue} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.ScalarValue.toObject = function(includeInstance, msg) {
  var f, obj = {
    integerValue: jspb.Message.getFieldWithDefault(msg, 1, 0),
    doubleValue: jspb.Message.getFloatingPointFieldWithDefault(msg, 2, 0.0),
    booleanValue: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    stringValue: jspb.Message.getFieldWithDefault(msg, 4, ""),
    specialValue: jspb.Message.getFieldWithDefault(msg, 5, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.ScalarValue}
 */
proto.caosdb.entity.v1.ScalarValue.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.ScalarValue;
  return proto.caosdb.entity.v1.ScalarValue.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.ScalarValue} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.ScalarValue}
 */
proto.caosdb.entity.v1.ScalarValue.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setIntegerValue(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setDoubleValue(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setBooleanValue(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setStringValue(value);
      break;
    case 5:
      var value = /** @type {!proto.caosdb.entity.v1.SpecialValue} */ (reader.readEnum());
      msg.setSpecialValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.ScalarValue.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.ScalarValue} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.ScalarValue.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeDouble(
      2,
      f
    );
  }
  f = /** @type {boolean} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeBool(
      3,
      f
    );
  }
  f = /** @type {string} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeString(
      4,
      f
    );
  }
  f = /** @type {!proto.caosdb.entity.v1.SpecialValue} */ (jspb.Message.getField(message, 5));
  if (f != null) {
    writer.writeEnum(
      5,
      f
    );
  }
};


/**
 * optional int64 integer_value = 1;
 * @return {number}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.getIntegerValue = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.caosdb.entity.v1.ScalarValue} returns this
 */
proto.caosdb.entity.v1.ScalarValue.prototype.setIntegerValue = function(value) {
  return jspb.Message.setOneofField(this, 1, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.caosdb.entity.v1.ScalarValue} returns this
 */
proto.caosdb.entity.v1.ScalarValue.prototype.clearIntegerValue = function() {
  return jspb.Message.setOneofField(this, 1, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.hasIntegerValue = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional double double_value = 2;
 * @return {number}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.getDoubleValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 2, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.caosdb.entity.v1.ScalarValue} returns this
 */
proto.caosdb.entity.v1.ScalarValue.prototype.setDoubleValue = function(value) {
  return jspb.Message.setOneofField(this, 2, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.caosdb.entity.v1.ScalarValue} returns this
 */
proto.caosdb.entity.v1.ScalarValue.prototype.clearDoubleValue = function() {
  return jspb.Message.setOneofField(this, 2, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.hasDoubleValue = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bool boolean_value = 3;
 * @return {boolean}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.getBooleanValue = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.caosdb.entity.v1.ScalarValue} returns this
 */
proto.caosdb.entity.v1.ScalarValue.prototype.setBooleanValue = function(value) {
  return jspb.Message.setOneofField(this, 3, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.caosdb.entity.v1.ScalarValue} returns this
 */
proto.caosdb.entity.v1.ScalarValue.prototype.clearBooleanValue = function() {
  return jspb.Message.setOneofField(this, 3, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.hasBooleanValue = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional string string_value = 4;
 * @return {string}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.getStringValue = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.ScalarValue} returns this
 */
proto.caosdb.entity.v1.ScalarValue.prototype.setStringValue = function(value) {
  return jspb.Message.setOneofField(this, 4, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.caosdb.entity.v1.ScalarValue} returns this
 */
proto.caosdb.entity.v1.ScalarValue.prototype.clearStringValue = function() {
  return jspb.Message.setOneofField(this, 4, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.hasStringValue = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional SpecialValue special_value = 5;
 * @return {!proto.caosdb.entity.v1.SpecialValue}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.getSpecialValue = function() {
  return /** @type {!proto.caosdb.entity.v1.SpecialValue} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {!proto.caosdb.entity.v1.SpecialValue} value
 * @return {!proto.caosdb.entity.v1.ScalarValue} returns this
 */
proto.caosdb.entity.v1.ScalarValue.prototype.setSpecialValue = function(value) {
  return jspb.Message.setOneofField(this, 5, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.caosdb.entity.v1.ScalarValue} returns this
 */
proto.caosdb.entity.v1.ScalarValue.prototype.clearSpecialValue = function() {
  return jspb.Message.setOneofField(this, 5, proto.caosdb.entity.v1.ScalarValue.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.ScalarValue.prototype.hasSpecialValue = function() {
  return jspb.Message.getField(this, 5) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.Value.oneofGroups_ = [[1,2]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.Value.ValueCase = {
  VALUE_NOT_SET: 0,
  SCALAR_VALUE: 1,
  LIST_VALUES: 2
};

/**
 * @return {proto.caosdb.entity.v1.Value.ValueCase}
 */
proto.caosdb.entity.v1.Value.prototype.getValueCase = function() {
  return /** @type {proto.caosdb.entity.v1.Value.ValueCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.Value.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.Value.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.Value.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.Value} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Value.toObject = function(includeInstance, msg) {
  var f, obj = {
    scalarValue: (f = msg.getScalarValue()) && proto.caosdb.entity.v1.ScalarValue.toObject(includeInstance, f),
    listValues: (f = msg.getListValues()) && proto.caosdb.entity.v1.CollectionValues.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.Value}
 */
proto.caosdb.entity.v1.Value.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.Value;
  return proto.caosdb.entity.v1.Value.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.Value} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.Value}
 */
proto.caosdb.entity.v1.Value.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.ScalarValue;
      reader.readMessage(value,proto.caosdb.entity.v1.ScalarValue.deserializeBinaryFromReader);
      msg.setScalarValue(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.CollectionValues;
      reader.readMessage(value,proto.caosdb.entity.v1.CollectionValues.deserializeBinaryFromReader);
      msg.setListValues(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.Value.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.Value.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.Value} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Value.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getScalarValue();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.ScalarValue.serializeBinaryToWriter
    );
  }
  f = message.getListValues();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.caosdb.entity.v1.CollectionValues.serializeBinaryToWriter
    );
  }
};


/**
 * optional ScalarValue scalar_value = 1;
 * @return {?proto.caosdb.entity.v1.ScalarValue}
 */
proto.caosdb.entity.v1.Value.prototype.getScalarValue = function() {
  return /** @type{?proto.caosdb.entity.v1.ScalarValue} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.ScalarValue, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.ScalarValue|undefined} value
 * @return {!proto.caosdb.entity.v1.Value} returns this
*/
proto.caosdb.entity.v1.Value.prototype.setScalarValue = function(value) {
  return jspb.Message.setOneofWrapperField(this, 1, proto.caosdb.entity.v1.Value.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.Value} returns this
 */
proto.caosdb.entity.v1.Value.prototype.clearScalarValue = function() {
  return this.setScalarValue(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.Value.prototype.hasScalarValue = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional CollectionValues list_values = 2;
 * @return {?proto.caosdb.entity.v1.CollectionValues}
 */
proto.caosdb.entity.v1.Value.prototype.getListValues = function() {
  return /** @type{?proto.caosdb.entity.v1.CollectionValues} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.CollectionValues, 2));
};


/**
 * @param {?proto.caosdb.entity.v1.CollectionValues|undefined} value
 * @return {!proto.caosdb.entity.v1.Value} returns this
*/
proto.caosdb.entity.v1.Value.prototype.setListValues = function(value) {
  return jspb.Message.setOneofWrapperField(this, 2, proto.caosdb.entity.v1.Value.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.Value} returns this
 */
proto.caosdb.entity.v1.Value.prototype.clearListValues = function() {
  return this.setListValues(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.Value.prototype.hasListValues = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.Message.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.Message.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.Message} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Message.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, 0),
    description: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.Message.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.Message;
  return proto.caosdb.entity.v1.Message.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.Message} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.Message.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setCode(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.Message.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.Message.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.Message} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Message.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional int32 code = 1;
 * @return {number}
 */
proto.caosdb.entity.v1.Message.prototype.getCode = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.caosdb.entity.v1.Message} returns this
 */
proto.caosdb.entity.v1.Message.prototype.setCode = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string description = 2;
 * @return {string}
 */
proto.caosdb.entity.v1.Message.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Message} returns this
 */
proto.caosdb.entity.v1.Message.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.Version.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.Version.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.Version} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Version.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.Version}
 */
proto.caosdb.entity.v1.Version.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.Version;
  return proto.caosdb.entity.v1.Version.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.Version} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.Version}
 */
proto.caosdb.entity.v1.Version.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.Version.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.Version.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.Version} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Version.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.Version.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Version} returns this
 */
proto.caosdb.entity.v1.Version.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.Entity.repeatedFields_ = [9,10];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.Entity.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.Entity.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.Entity} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Entity.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    name: jspb.Message.getFieldWithDefault(msg, 2, ""),
    description: jspb.Message.getFieldWithDefault(msg, 3, ""),
    version: (f = msg.getVersion()) && proto.caosdb.entity.v1.Version.toObject(includeInstance, f),
    role: jspb.Message.getFieldWithDefault(msg, 5, 0),
    unit: jspb.Message.getFieldWithDefault(msg, 6, ""),
    dataType: (f = msg.getDataType()) && proto.caosdb.entity.v1.DataType.toObject(includeInstance, f),
    value: (f = msg.getValue()) && proto.caosdb.entity.v1.Value.toObject(includeInstance, f),
    propertiesList: jspb.Message.toObjectList(msg.getPropertiesList(),
    proto.caosdb.entity.v1.Property.toObject, includeInstance),
    parentsList: jspb.Message.toObjectList(msg.getParentsList(),
    proto.caosdb.entity.v1.Parent.toObject, includeInstance),
    fileDescriptor: (f = msg.getFileDescriptor()) && proto.caosdb.entity.v1.FileDescriptor.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.Entity}
 */
proto.caosdb.entity.v1.Entity.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.Entity;
  return proto.caosdb.entity.v1.Entity.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.Entity} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.Entity}
 */
proto.caosdb.entity.v1.Entity.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.Version;
      reader.readMessage(value,proto.caosdb.entity.v1.Version.deserializeBinaryFromReader);
      msg.setVersion(value);
      break;
    case 5:
      var value = /** @type {!proto.caosdb.entity.v1.EntityRole} */ (reader.readEnum());
      msg.setRole(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setUnit(value);
      break;
    case 7:
      var value = new proto.caosdb.entity.v1.DataType;
      reader.readMessage(value,proto.caosdb.entity.v1.DataType.deserializeBinaryFromReader);
      msg.setDataType(value);
      break;
    case 8:
      var value = new proto.caosdb.entity.v1.Value;
      reader.readMessage(value,proto.caosdb.entity.v1.Value.deserializeBinaryFromReader);
      msg.setValue(value);
      break;
    case 9:
      var value = new proto.caosdb.entity.v1.Property;
      reader.readMessage(value,proto.caosdb.entity.v1.Property.deserializeBinaryFromReader);
      msg.addProperties(value);
      break;
    case 10:
      var value = new proto.caosdb.entity.v1.Parent;
      reader.readMessage(value,proto.caosdb.entity.v1.Parent.deserializeBinaryFromReader);
      msg.addParents(value);
      break;
    case 14:
      var value = new proto.caosdb.entity.v1.FileDescriptor;
      reader.readMessage(value,proto.caosdb.entity.v1.FileDescriptor.deserializeBinaryFromReader);
      msg.setFileDescriptor(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.Entity.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.Entity.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.Entity} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Entity.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getVersion();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.caosdb.entity.v1.Version.serializeBinaryToWriter
    );
  }
  f = message.getRole();
  if (f !== 0.0) {
    writer.writeEnum(
      5,
      f
    );
  }
  f = message.getUnit();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getDataType();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.caosdb.entity.v1.DataType.serializeBinaryToWriter
    );
  }
  f = message.getValue();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      proto.caosdb.entity.v1.Value.serializeBinaryToWriter
    );
  }
  f = message.getPropertiesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      9,
      f,
      proto.caosdb.entity.v1.Property.serializeBinaryToWriter
    );
  }
  f = message.getParentsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      10,
      f,
      proto.caosdb.entity.v1.Parent.serializeBinaryToWriter
    );
  }
  f = message.getFileDescriptor();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      proto.caosdb.entity.v1.FileDescriptor.serializeBinaryToWriter
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.Entity.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.caosdb.entity.v1.Entity.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string description = 3;
 * @return {string}
 */
proto.caosdb.entity.v1.Entity.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional Version version = 4;
 * @return {?proto.caosdb.entity.v1.Version}
 */
proto.caosdb.entity.v1.Entity.prototype.getVersion = function() {
  return /** @type{?proto.caosdb.entity.v1.Version} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.Version, 4));
};


/**
 * @param {?proto.caosdb.entity.v1.Version|undefined} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
*/
proto.caosdb.entity.v1.Entity.prototype.setVersion = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.clearVersion = function() {
  return this.setVersion(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.Entity.prototype.hasVersion = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional EntityRole role = 5;
 * @return {!proto.caosdb.entity.v1.EntityRole}
 */
proto.caosdb.entity.v1.Entity.prototype.getRole = function() {
  return /** @type {!proto.caosdb.entity.v1.EntityRole} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {!proto.caosdb.entity.v1.EntityRole} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.setRole = function(value) {
  return jspb.Message.setProto3EnumField(this, 5, value);
};


/**
 * optional string unit = 6;
 * @return {string}
 */
proto.caosdb.entity.v1.Entity.prototype.getUnit = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.setUnit = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional DataType data_type = 7;
 * @return {?proto.caosdb.entity.v1.DataType}
 */
proto.caosdb.entity.v1.Entity.prototype.getDataType = function() {
  return /** @type{?proto.caosdb.entity.v1.DataType} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.DataType, 7));
};


/**
 * @param {?proto.caosdb.entity.v1.DataType|undefined} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
*/
proto.caosdb.entity.v1.Entity.prototype.setDataType = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.clearDataType = function() {
  return this.setDataType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.Entity.prototype.hasDataType = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional Value value = 8;
 * @return {?proto.caosdb.entity.v1.Value}
 */
proto.caosdb.entity.v1.Entity.prototype.getValue = function() {
  return /** @type{?proto.caosdb.entity.v1.Value} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.Value, 8));
};


/**
 * @param {?proto.caosdb.entity.v1.Value|undefined} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
*/
proto.caosdb.entity.v1.Entity.prototype.setValue = function(value) {
  return jspb.Message.setWrapperField(this, 8, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.clearValue = function() {
  return this.setValue(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.Entity.prototype.hasValue = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * repeated Property properties = 9;
 * @return {!Array<!proto.caosdb.entity.v1.Property>}
 */
proto.caosdb.entity.v1.Entity.prototype.getPropertiesList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Property>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Property, 9));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Property>} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
*/
proto.caosdb.entity.v1.Entity.prototype.setPropertiesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 9, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Property=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Property}
 */
proto.caosdb.entity.v1.Entity.prototype.addProperties = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 9, opt_value, proto.caosdb.entity.v1.Property, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.clearPropertiesList = function() {
  return this.setPropertiesList([]);
};


/**
 * repeated Parent parents = 10;
 * @return {!Array<!proto.caosdb.entity.v1.Parent>}
 */
proto.caosdb.entity.v1.Entity.prototype.getParentsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Parent>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Parent, 10));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Parent>} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
*/
proto.caosdb.entity.v1.Entity.prototype.setParentsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 10, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Parent=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Parent}
 */
proto.caosdb.entity.v1.Entity.prototype.addParents = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 10, opt_value, proto.caosdb.entity.v1.Parent, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.clearParentsList = function() {
  return this.setParentsList([]);
};


/**
 * optional FileDescriptor file_descriptor = 14;
 * @return {?proto.caosdb.entity.v1.FileDescriptor}
 */
proto.caosdb.entity.v1.Entity.prototype.getFileDescriptor = function() {
  return /** @type{?proto.caosdb.entity.v1.FileDescriptor} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.FileDescriptor, 14));
};


/**
 * @param {?proto.caosdb.entity.v1.FileDescriptor|undefined} value
 * @return {!proto.caosdb.entity.v1.Entity} returns this
*/
proto.caosdb.entity.v1.Entity.prototype.setFileDescriptor = function(value) {
  return jspb.Message.setWrapperField(this, 14, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.Entity} returns this
 */
proto.caosdb.entity.v1.Entity.prototype.clearFileDescriptor = function() {
  return this.setFileDescriptor(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.Entity.prototype.hasFileDescriptor = function() {
  return jspb.Message.getField(this, 14) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.FileDescriptor.repeatedFields_ = [4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.FileDescriptor.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.FileDescriptor} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileDescriptor.toObject = function(includeInstance, msg) {
  var f, obj = {
    entityId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    path: jspb.Message.getFieldWithDefault(msg, 2, ""),
    size: jspb.Message.getFieldWithDefault(msg, 3, 0),
    hashesList: jspb.Message.toObjectList(msg.getHashesList(),
    proto.caosdb.entity.v1.Hash.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.FileDescriptor}
 */
proto.caosdb.entity.v1.FileDescriptor.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.FileDescriptor;
  return proto.caosdb.entity.v1.FileDescriptor.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.FileDescriptor} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.FileDescriptor}
 */
proto.caosdb.entity.v1.FileDescriptor.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setEntityId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPath(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setSize(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.Hash;
      reader.readMessage(value,proto.caosdb.entity.v1.Hash.deserializeBinaryFromReader);
      msg.addHashes(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.FileDescriptor.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.FileDescriptor} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileDescriptor.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEntityId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPath();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getSize();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
  f = message.getHashesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.caosdb.entity.v1.Hash.serializeBinaryToWriter
    );
  }
};


/**
 * optional string entity_id = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.getEntityId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.FileDescriptor} returns this
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.setEntityId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string path = 2;
 * @return {string}
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.getPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.FileDescriptor} returns this
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.setPath = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional int64 size = 3;
 * @return {number}
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.getSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.caosdb.entity.v1.FileDescriptor} returns this
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.setSize = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * repeated Hash hashes = 4;
 * @return {!Array<!proto.caosdb.entity.v1.Hash>}
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.getHashesList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Hash>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Hash, 4));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Hash>} value
 * @return {!proto.caosdb.entity.v1.FileDescriptor} returns this
*/
proto.caosdb.entity.v1.FileDescriptor.prototype.setHashesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Hash=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Hash}
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.addHashes = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.caosdb.entity.v1.Hash, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.FileDescriptor} returns this
 */
proto.caosdb.entity.v1.FileDescriptor.prototype.clearHashesList = function() {
  return this.setHashesList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.Hash.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.Hash.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.Hash} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Hash.toObject = function(includeInstance, msg) {
  var f, obj = {
    algorithm: jspb.Message.getFieldWithDefault(msg, 1, ""),
    value: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.Hash}
 */
proto.caosdb.entity.v1.Hash.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.Hash;
  return proto.caosdb.entity.v1.Hash.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.Hash} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.Hash}
 */
proto.caosdb.entity.v1.Hash.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAlgorithm(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.Hash.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.Hash.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.Hash} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Hash.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAlgorithm();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getValue();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string algorithm = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.Hash.prototype.getAlgorithm = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Hash} returns this
 */
proto.caosdb.entity.v1.Hash.prototype.setAlgorithm = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string value = 2;
 * @return {string}
 */
proto.caosdb.entity.v1.Hash.prototype.getValue = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Hash} returns this
 */
proto.caosdb.entity.v1.Hash.prototype.setValue = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.Property.repeatedFields_ = [8,9,10];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.Property.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.Property.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.Property} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Property.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    name: jspb.Message.getFieldWithDefault(msg, 2, ""),
    description: jspb.Message.getFieldWithDefault(msg, 3, ""),
    value: (f = msg.getValue()) && proto.caosdb.entity.v1.Value.toObject(includeInstance, f),
    importance: jspb.Message.getFieldWithDefault(msg, 5, 0),
    unit: jspb.Message.getFieldWithDefault(msg, 6, ""),
    dataType: (f = msg.getDataType()) && proto.caosdb.entity.v1.DataType.toObject(includeInstance, f),
    errorsList: jspb.Message.toObjectList(msg.getErrorsList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance),
    warningsList: jspb.Message.toObjectList(msg.getWarningsList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance),
    infosList: jspb.Message.toObjectList(msg.getInfosList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.Property}
 */
proto.caosdb.entity.v1.Property.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.Property;
  return proto.caosdb.entity.v1.Property.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.Property} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.Property}
 */
proto.caosdb.entity.v1.Property.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.Value;
      reader.readMessage(value,proto.caosdb.entity.v1.Value.deserializeBinaryFromReader);
      msg.setValue(value);
      break;
    case 5:
      var value = /** @type {!proto.caosdb.entity.v1.Importance} */ (reader.readEnum());
      msg.setImportance(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setUnit(value);
      break;
    case 7:
      var value = new proto.caosdb.entity.v1.DataType;
      reader.readMessage(value,proto.caosdb.entity.v1.DataType.deserializeBinaryFromReader);
      msg.setDataType(value);
      break;
    case 8:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addErrors(value);
      break;
    case 9:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addWarnings(value);
      break;
    case 10:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addInfos(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.Property.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.Property.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.Property} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Property.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getValue();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.caosdb.entity.v1.Value.serializeBinaryToWriter
    );
  }
  f = message.getImportance();
  if (f !== 0.0) {
    writer.writeEnum(
      5,
      f
    );
  }
  f = message.getUnit();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getDataType();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.caosdb.entity.v1.DataType.serializeBinaryToWriter
    );
  }
  f = message.getErrorsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      8,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
  f = message.getWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      9,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
  f = message.getInfosList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      10,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.Property.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Property} returns this
 */
proto.caosdb.entity.v1.Property.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.caosdb.entity.v1.Property.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Property} returns this
 */
proto.caosdb.entity.v1.Property.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string description = 3;
 * @return {string}
 */
proto.caosdb.entity.v1.Property.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Property} returns this
 */
proto.caosdb.entity.v1.Property.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional Value value = 4;
 * @return {?proto.caosdb.entity.v1.Value}
 */
proto.caosdb.entity.v1.Property.prototype.getValue = function() {
  return /** @type{?proto.caosdb.entity.v1.Value} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.Value, 4));
};


/**
 * @param {?proto.caosdb.entity.v1.Value|undefined} value
 * @return {!proto.caosdb.entity.v1.Property} returns this
*/
proto.caosdb.entity.v1.Property.prototype.setValue = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.Property} returns this
 */
proto.caosdb.entity.v1.Property.prototype.clearValue = function() {
  return this.setValue(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.Property.prototype.hasValue = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional Importance importance = 5;
 * @return {!proto.caosdb.entity.v1.Importance}
 */
proto.caosdb.entity.v1.Property.prototype.getImportance = function() {
  return /** @type {!proto.caosdb.entity.v1.Importance} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {!proto.caosdb.entity.v1.Importance} value
 * @return {!proto.caosdb.entity.v1.Property} returns this
 */
proto.caosdb.entity.v1.Property.prototype.setImportance = function(value) {
  return jspb.Message.setProto3EnumField(this, 5, value);
};


/**
 * optional string unit = 6;
 * @return {string}
 */
proto.caosdb.entity.v1.Property.prototype.getUnit = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Property} returns this
 */
proto.caosdb.entity.v1.Property.prototype.setUnit = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional DataType data_type = 7;
 * @return {?proto.caosdb.entity.v1.DataType}
 */
proto.caosdb.entity.v1.Property.prototype.getDataType = function() {
  return /** @type{?proto.caosdb.entity.v1.DataType} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.DataType, 7));
};


/**
 * @param {?proto.caosdb.entity.v1.DataType|undefined} value
 * @return {!proto.caosdb.entity.v1.Property} returns this
*/
proto.caosdb.entity.v1.Property.prototype.setDataType = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.Property} returns this
 */
proto.caosdb.entity.v1.Property.prototype.clearDataType = function() {
  return this.setDataType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.Property.prototype.hasDataType = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * repeated Message errors = 8;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.Property.prototype.getErrorsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 8));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.Property} returns this
*/
proto.caosdb.entity.v1.Property.prototype.setErrorsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 8, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.Property.prototype.addErrors = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 8, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.Property} returns this
 */
proto.caosdb.entity.v1.Property.prototype.clearErrorsList = function() {
  return this.setErrorsList([]);
};


/**
 * repeated Message warnings = 9;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.Property.prototype.getWarningsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 9));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.Property} returns this
*/
proto.caosdb.entity.v1.Property.prototype.setWarningsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 9, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.Property.prototype.addWarnings = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 9, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.Property} returns this
 */
proto.caosdb.entity.v1.Property.prototype.clearWarningsList = function() {
  return this.setWarningsList([]);
};


/**
 * repeated Message infos = 10;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.Property.prototype.getInfosList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 10));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.Property} returns this
*/
proto.caosdb.entity.v1.Property.prototype.setInfosList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 10, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.Property.prototype.addInfos = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 10, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.Property} returns this
 */
proto.caosdb.entity.v1.Property.prototype.clearInfosList = function() {
  return this.setInfosList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.Parent.repeatedFields_ = [4,5,6];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.Parent.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.Parent.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.Parent} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Parent.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    name: jspb.Message.getFieldWithDefault(msg, 2, ""),
    description: jspb.Message.getFieldWithDefault(msg, 3, ""),
    errorsList: jspb.Message.toObjectList(msg.getErrorsList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance),
    warningsList: jspb.Message.toObjectList(msg.getWarningsList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance),
    infosList: jspb.Message.toObjectList(msg.getInfosList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.Parent}
 */
proto.caosdb.entity.v1.Parent.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.Parent;
  return proto.caosdb.entity.v1.Parent.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.Parent} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.Parent}
 */
proto.caosdb.entity.v1.Parent.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addErrors(value);
      break;
    case 5:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addWarnings(value);
      break;
    case 6:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addInfos(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.Parent.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.Parent.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.Parent} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Parent.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getErrorsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
  f = message.getWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
  f = message.getInfosList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      6,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.Parent.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Parent} returns this
 */
proto.caosdb.entity.v1.Parent.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.caosdb.entity.v1.Parent.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Parent} returns this
 */
proto.caosdb.entity.v1.Parent.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string description = 3;
 * @return {string}
 */
proto.caosdb.entity.v1.Parent.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Parent} returns this
 */
proto.caosdb.entity.v1.Parent.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * repeated Message errors = 4;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.Parent.prototype.getErrorsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 4));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.Parent} returns this
*/
proto.caosdb.entity.v1.Parent.prototype.setErrorsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.Parent.prototype.addErrors = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.Parent} returns this
 */
proto.caosdb.entity.v1.Parent.prototype.clearErrorsList = function() {
  return this.setErrorsList([]);
};


/**
 * repeated Message warnings = 5;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.Parent.prototype.getWarningsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 5));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.Parent} returns this
*/
proto.caosdb.entity.v1.Parent.prototype.setWarningsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.Parent.prototype.addWarnings = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.Parent} returns this
 */
proto.caosdb.entity.v1.Parent.prototype.clearWarningsList = function() {
  return this.setWarningsList([]);
};


/**
 * repeated Message infos = 6;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.Parent.prototype.getInfosList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 6));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.Parent} returns this
*/
proto.caosdb.entity.v1.Parent.prototype.setInfosList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 6, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.Parent.prototype.addInfos = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 6, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.Parent} returns this
 */
proto.caosdb.entity.v1.Parent.prototype.clearInfosList = function() {
  return this.setInfosList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.EntityRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.EntityRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.EntityRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.EntityRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    entity: (f = msg.getEntity()) && proto.caosdb.entity.v1.Entity.toObject(includeInstance, f),
    uploadId: (f = msg.getUploadId()) && proto.caosdb.entity.v1.FileTransmissionId.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.EntityRequest}
 */
proto.caosdb.entity.v1.EntityRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.EntityRequest;
  return proto.caosdb.entity.v1.EntityRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.EntityRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.EntityRequest}
 */
proto.caosdb.entity.v1.EntityRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.Entity;
      reader.readMessage(value,proto.caosdb.entity.v1.Entity.deserializeBinaryFromReader);
      msg.setEntity(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.FileTransmissionId;
      reader.readMessage(value,proto.caosdb.entity.v1.FileTransmissionId.deserializeBinaryFromReader);
      msg.setUploadId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.EntityRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.EntityRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.EntityRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.EntityRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEntity();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.Entity.serializeBinaryToWriter
    );
  }
  f = message.getUploadId();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.caosdb.entity.v1.FileTransmissionId.serializeBinaryToWriter
    );
  }
};


/**
 * optional Entity entity = 1;
 * @return {?proto.caosdb.entity.v1.Entity}
 */
proto.caosdb.entity.v1.EntityRequest.prototype.getEntity = function() {
  return /** @type{?proto.caosdb.entity.v1.Entity} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.Entity, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.Entity|undefined} value
 * @return {!proto.caosdb.entity.v1.EntityRequest} returns this
*/
proto.caosdb.entity.v1.EntityRequest.prototype.setEntity = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.EntityRequest} returns this
 */
proto.caosdb.entity.v1.EntityRequest.prototype.clearEntity = function() {
  return this.setEntity(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.EntityRequest.prototype.hasEntity = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional FileTransmissionId upload_id = 2;
 * @return {?proto.caosdb.entity.v1.FileTransmissionId}
 */
proto.caosdb.entity.v1.EntityRequest.prototype.getUploadId = function() {
  return /** @type{?proto.caosdb.entity.v1.FileTransmissionId} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.FileTransmissionId, 2));
};


/**
 * @param {?proto.caosdb.entity.v1.FileTransmissionId|undefined} value
 * @return {!proto.caosdb.entity.v1.EntityRequest} returns this
*/
proto.caosdb.entity.v1.EntityRequest.prototype.setUploadId = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.EntityRequest} returns this
 */
proto.caosdb.entity.v1.EntityRequest.prototype.clearUploadId = function() {
  return this.setUploadId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.EntityRequest.prototype.hasUploadId = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.EntityResponse.repeatedFields_ = [3,4,5];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.EntityResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.EntityResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.EntityResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    entity: (f = msg.getEntity()) && proto.caosdb.entity.v1.Entity.toObject(includeInstance, f),
    downloadId: (f = msg.getDownloadId()) && proto.caosdb.entity.v1.FileTransmissionId.toObject(includeInstance, f),
    errorsList: jspb.Message.toObjectList(msg.getErrorsList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance),
    warningsList: jspb.Message.toObjectList(msg.getWarningsList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance),
    infosList: jspb.Message.toObjectList(msg.getInfosList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.EntityResponse}
 */
proto.caosdb.entity.v1.EntityResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.EntityResponse;
  return proto.caosdb.entity.v1.EntityResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.EntityResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.EntityResponse}
 */
proto.caosdb.entity.v1.EntityResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.Entity;
      reader.readMessage(value,proto.caosdb.entity.v1.Entity.deserializeBinaryFromReader);
      msg.setEntity(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.FileTransmissionId;
      reader.readMessage(value,proto.caosdb.entity.v1.FileTransmissionId.deserializeBinaryFromReader);
      msg.setDownloadId(value);
      break;
    case 3:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addErrors(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addWarnings(value);
      break;
    case 5:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addInfos(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.EntityResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.EntityResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.EntityResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEntity();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.Entity.serializeBinaryToWriter
    );
  }
  f = message.getDownloadId();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.caosdb.entity.v1.FileTransmissionId.serializeBinaryToWriter
    );
  }
  f = message.getErrorsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
  f = message.getWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
  f = message.getInfosList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
};


/**
 * optional Entity entity = 1;
 * @return {?proto.caosdb.entity.v1.Entity}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.getEntity = function() {
  return /** @type{?proto.caosdb.entity.v1.Entity} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.Entity, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.Entity|undefined} value
 * @return {!proto.caosdb.entity.v1.EntityResponse} returns this
*/
proto.caosdb.entity.v1.EntityResponse.prototype.setEntity = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.EntityResponse} returns this
 */
proto.caosdb.entity.v1.EntityResponse.prototype.clearEntity = function() {
  return this.setEntity(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.hasEntity = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional FileTransmissionId download_id = 2;
 * @return {?proto.caosdb.entity.v1.FileTransmissionId}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.getDownloadId = function() {
  return /** @type{?proto.caosdb.entity.v1.FileTransmissionId} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.FileTransmissionId, 2));
};


/**
 * @param {?proto.caosdb.entity.v1.FileTransmissionId|undefined} value
 * @return {!proto.caosdb.entity.v1.EntityResponse} returns this
*/
proto.caosdb.entity.v1.EntityResponse.prototype.setDownloadId = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.EntityResponse} returns this
 */
proto.caosdb.entity.v1.EntityResponse.prototype.clearDownloadId = function() {
  return this.setDownloadId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.hasDownloadId = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * repeated Message errors = 3;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.getErrorsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 3));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.EntityResponse} returns this
*/
proto.caosdb.entity.v1.EntityResponse.prototype.setErrorsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.addErrors = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.EntityResponse} returns this
 */
proto.caosdb.entity.v1.EntityResponse.prototype.clearErrorsList = function() {
  return this.setErrorsList([]);
};


/**
 * repeated Message warnings = 4;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.getWarningsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 4));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.EntityResponse} returns this
*/
proto.caosdb.entity.v1.EntityResponse.prototype.setWarningsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.addWarnings = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.EntityResponse} returns this
 */
proto.caosdb.entity.v1.EntityResponse.prototype.clearWarningsList = function() {
  return this.setWarningsList([]);
};


/**
 * repeated Message infos = 5;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.getInfosList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 5));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.EntityResponse} returns this
*/
proto.caosdb.entity.v1.EntityResponse.prototype.setInfosList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.EntityResponse.prototype.addInfos = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.EntityResponse} returns this
 */
proto.caosdb.entity.v1.EntityResponse.prototype.clearInfosList = function() {
  return this.setInfosList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.IdResponse.repeatedFields_ = [2,3,4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.IdResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.IdResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.IdResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.IdResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    version: (f = msg.getVersion()) && proto.caosdb.entity.v1.Version.toObject(includeInstance, f),
    errorsList: jspb.Message.toObjectList(msg.getErrorsList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance),
    warningsList: jspb.Message.toObjectList(msg.getWarningsList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance),
    infosList: jspb.Message.toObjectList(msg.getInfosList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.IdResponse}
 */
proto.caosdb.entity.v1.IdResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.IdResponse;
  return proto.caosdb.entity.v1.IdResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.IdResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.IdResponse}
 */
proto.caosdb.entity.v1.IdResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 5:
      var value = new proto.caosdb.entity.v1.Version;
      reader.readMessage(value,proto.caosdb.entity.v1.Version.deserializeBinaryFromReader);
      msg.setVersion(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addErrors(value);
      break;
    case 3:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addWarnings(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addInfos(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.IdResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.IdResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.IdResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.IdResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getVersion();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.caosdb.entity.v1.Version.serializeBinaryToWriter
    );
  }
  f = message.getErrorsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
  f = message.getWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
  f = message.getInfosList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.IdResponse.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.IdResponse} returns this
 */
proto.caosdb.entity.v1.IdResponse.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional Version version = 5;
 * @return {?proto.caosdb.entity.v1.Version}
 */
proto.caosdb.entity.v1.IdResponse.prototype.getVersion = function() {
  return /** @type{?proto.caosdb.entity.v1.Version} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.Version, 5));
};


/**
 * @param {?proto.caosdb.entity.v1.Version|undefined} value
 * @return {!proto.caosdb.entity.v1.IdResponse} returns this
*/
proto.caosdb.entity.v1.IdResponse.prototype.setVersion = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.IdResponse} returns this
 */
proto.caosdb.entity.v1.IdResponse.prototype.clearVersion = function() {
  return this.setVersion(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.IdResponse.prototype.hasVersion = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * repeated Message errors = 2;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.IdResponse.prototype.getErrorsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 2));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.IdResponse} returns this
*/
proto.caosdb.entity.v1.IdResponse.prototype.setErrorsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.IdResponse.prototype.addErrors = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.IdResponse} returns this
 */
proto.caosdb.entity.v1.IdResponse.prototype.clearErrorsList = function() {
  return this.setErrorsList([]);
};


/**
 * repeated Message warnings = 3;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.IdResponse.prototype.getWarningsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 3));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.IdResponse} returns this
*/
proto.caosdb.entity.v1.IdResponse.prototype.setWarningsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.IdResponse.prototype.addWarnings = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.IdResponse} returns this
 */
proto.caosdb.entity.v1.IdResponse.prototype.clearWarningsList = function() {
  return this.setWarningsList([]);
};


/**
 * repeated Message infos = 4;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.IdResponse.prototype.getInfosList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 4));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.IdResponse} returns this
*/
proto.caosdb.entity.v1.IdResponse.prototype.setInfosList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.IdResponse.prototype.addInfos = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.IdResponse} returns this
 */
proto.caosdb.entity.v1.IdResponse.prototype.clearInfosList = function() {
  return this.setInfosList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.Query.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.Query.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.Query} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Query.toObject = function(includeInstance, msg) {
  var f, obj = {
    query: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.Query}
 */
proto.caosdb.entity.v1.Query.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.Query;
  return proto.caosdb.entity.v1.Query.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.Query} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.Query}
 */
proto.caosdb.entity.v1.Query.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setQuery(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.Query.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.Query.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.Query} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.Query.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getQuery();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string query = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.Query.prototype.getQuery = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.Query} returns this
 */
proto.caosdb.entity.v1.Query.prototype.setQuery = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.FindQueryResult.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.FindQueryResult.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.FindQueryResult.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.FindQueryResult} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FindQueryResult.toObject = function(includeInstance, msg) {
  var f, obj = {
    resultSetList: jspb.Message.toObjectList(msg.getResultSetList(),
    proto.caosdb.entity.v1.EntityResponse.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.FindQueryResult}
 */
proto.caosdb.entity.v1.FindQueryResult.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.FindQueryResult;
  return proto.caosdb.entity.v1.FindQueryResult.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.FindQueryResult} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.FindQueryResult}
 */
proto.caosdb.entity.v1.FindQueryResult.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.EntityResponse;
      reader.readMessage(value,proto.caosdb.entity.v1.EntityResponse.deserializeBinaryFromReader);
      msg.addResultSet(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.FindQueryResult.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.FindQueryResult.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.FindQueryResult} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FindQueryResult.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getResultSetList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.caosdb.entity.v1.EntityResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated EntityResponse result_set = 1;
 * @return {!Array<!proto.caosdb.entity.v1.EntityResponse>}
 */
proto.caosdb.entity.v1.FindQueryResult.prototype.getResultSetList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.EntityResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.EntityResponse, 1));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.EntityResponse>} value
 * @return {!proto.caosdb.entity.v1.FindQueryResult} returns this
*/
proto.caosdb.entity.v1.FindQueryResult.prototype.setResultSetList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.caosdb.entity.v1.EntityResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.EntityResponse}
 */
proto.caosdb.entity.v1.FindQueryResult.prototype.addResultSet = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.caosdb.entity.v1.EntityResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.FindQueryResult} returns this
 */
proto.caosdb.entity.v1.FindQueryResult.prototype.clearResultSetList = function() {
  return this.setResultSetList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.SelectQueryRows.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.SelectQueryRows.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.SelectQueryRows.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.SelectQueryRows} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.SelectQueryRows.toObject = function(includeInstance, msg) {
  var f, obj = {
    cellsList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.SelectQueryRows}
 */
proto.caosdb.entity.v1.SelectQueryRows.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.SelectQueryRows;
  return proto.caosdb.entity.v1.SelectQueryRows.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.SelectQueryRows} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.SelectQueryRows}
 */
proto.caosdb.entity.v1.SelectQueryRows.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.addCells(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.SelectQueryRows.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.SelectQueryRows.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.SelectQueryRows} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.SelectQueryRows.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCellsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      1,
      f
    );
  }
};


/**
 * repeated string cells = 1;
 * @return {!Array<string>}
 */
proto.caosdb.entity.v1.SelectQueryRows.prototype.getCellsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.caosdb.entity.v1.SelectQueryRows} returns this
 */
proto.caosdb.entity.v1.SelectQueryRows.prototype.setCellsList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.SelectQueryRows} returns this
 */
proto.caosdb.entity.v1.SelectQueryRows.prototype.addCells = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.SelectQueryRows} returns this
 */
proto.caosdb.entity.v1.SelectQueryRows.prototype.clearCellsList = function() {
  return this.setCellsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.SelectQueryResult.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.SelectQueryResult.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.SelectQueryResult.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.SelectQueryResult} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.SelectQueryResult.toObject = function(includeInstance, msg) {
  var f, obj = {
    header: (f = msg.getHeader()) && proto.caosdb.entity.v1.SelectQueryRows.toObject(includeInstance, f),
    dataRowsList: jspb.Message.toObjectList(msg.getDataRowsList(),
    proto.caosdb.entity.v1.SelectQueryRows.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.SelectQueryResult}
 */
proto.caosdb.entity.v1.SelectQueryResult.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.SelectQueryResult;
  return proto.caosdb.entity.v1.SelectQueryResult.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.SelectQueryResult} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.SelectQueryResult}
 */
proto.caosdb.entity.v1.SelectQueryResult.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.SelectQueryRows;
      reader.readMessage(value,proto.caosdb.entity.v1.SelectQueryRows.deserializeBinaryFromReader);
      msg.setHeader(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.SelectQueryRows;
      reader.readMessage(value,proto.caosdb.entity.v1.SelectQueryRows.deserializeBinaryFromReader);
      msg.addDataRows(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.SelectQueryResult.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.SelectQueryResult.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.SelectQueryResult} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.SelectQueryResult.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getHeader();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.SelectQueryRows.serializeBinaryToWriter
    );
  }
  f = message.getDataRowsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.caosdb.entity.v1.SelectQueryRows.serializeBinaryToWriter
    );
  }
};


/**
 * optional SelectQueryRows header = 1;
 * @return {?proto.caosdb.entity.v1.SelectQueryRows}
 */
proto.caosdb.entity.v1.SelectQueryResult.prototype.getHeader = function() {
  return /** @type{?proto.caosdb.entity.v1.SelectQueryRows} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.SelectQueryRows, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.SelectQueryRows|undefined} value
 * @return {!proto.caosdb.entity.v1.SelectQueryResult} returns this
*/
proto.caosdb.entity.v1.SelectQueryResult.prototype.setHeader = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.SelectQueryResult} returns this
 */
proto.caosdb.entity.v1.SelectQueryResult.prototype.clearHeader = function() {
  return this.setHeader(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.SelectQueryResult.prototype.hasHeader = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated SelectQueryRows data_rows = 2;
 * @return {!Array<!proto.caosdb.entity.v1.SelectQueryRows>}
 */
proto.caosdb.entity.v1.SelectQueryResult.prototype.getDataRowsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.SelectQueryRows>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.SelectQueryRows, 2));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.SelectQueryRows>} value
 * @return {!proto.caosdb.entity.v1.SelectQueryResult} returns this
*/
proto.caosdb.entity.v1.SelectQueryResult.prototype.setDataRowsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.caosdb.entity.v1.SelectQueryRows=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.SelectQueryRows}
 */
proto.caosdb.entity.v1.SelectQueryResult.prototype.addDataRows = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.caosdb.entity.v1.SelectQueryRows, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.SelectQueryResult} returns this
 */
proto.caosdb.entity.v1.SelectQueryResult.prototype.clearDataRowsList = function() {
  return this.setDataRowsList([]);
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.RetrieveRequest.oneofGroups_ = [[1,2]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.RetrieveRequest.WrappedRequestCase = {
  WRAPPED_REQUEST_NOT_SET: 0,
  ID: 1,
  QUERY: 2
};

/**
 * @return {proto.caosdb.entity.v1.RetrieveRequest.WrappedRequestCase}
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.getWrappedRequestCase = function() {
  return /** @type {proto.caosdb.entity.v1.RetrieveRequest.WrappedRequestCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.RetrieveRequest.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.RetrieveRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.RetrieveRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.RetrieveRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    query: (f = msg.getQuery()) && proto.caosdb.entity.v1.Query.toObject(includeInstance, f),
    registerFileDownload: jspb.Message.getBooleanFieldWithDefault(msg, 3, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.RetrieveRequest}
 */
proto.caosdb.entity.v1.RetrieveRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.RetrieveRequest;
  return proto.caosdb.entity.v1.RetrieveRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.RetrieveRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.RetrieveRequest}
 */
proto.caosdb.entity.v1.RetrieveRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.Query;
      reader.readMessage(value,proto.caosdb.entity.v1.Query.deserializeBinaryFromReader);
      msg.setQuery(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setRegisterFileDownload(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.RetrieveRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.RetrieveRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.RetrieveRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getQuery();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.caosdb.entity.v1.Query.serializeBinaryToWriter
    );
  }
  f = message.getRegisterFileDownload();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.RetrieveRequest} returns this
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.setId = function(value) {
  return jspb.Message.setOneofField(this, 1, proto.caosdb.entity.v1.RetrieveRequest.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.caosdb.entity.v1.RetrieveRequest} returns this
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.clearId = function() {
  return jspb.Message.setOneofField(this, 1, proto.caosdb.entity.v1.RetrieveRequest.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.hasId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional Query query = 2;
 * @return {?proto.caosdb.entity.v1.Query}
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.getQuery = function() {
  return /** @type{?proto.caosdb.entity.v1.Query} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.Query, 2));
};


/**
 * @param {?proto.caosdb.entity.v1.Query|undefined} value
 * @return {!proto.caosdb.entity.v1.RetrieveRequest} returns this
*/
proto.caosdb.entity.v1.RetrieveRequest.prototype.setQuery = function(value) {
  return jspb.Message.setOneofWrapperField(this, 2, proto.caosdb.entity.v1.RetrieveRequest.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.RetrieveRequest} returns this
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.clearQuery = function() {
  return this.setQuery(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.hasQuery = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bool register_file_download = 3;
 * @return {boolean}
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.getRegisterFileDownload = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.caosdb.entity.v1.RetrieveRequest} returns this
 */
proto.caosdb.entity.v1.RetrieveRequest.prototype.setRegisterFileDownload = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.RetrieveResponse.oneofGroups_ = [[1,2,3,4]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.RetrieveResponse.RetrieveResponseCase = {
  RETRIEVE_RESPONSE_NOT_SET: 0,
  ENTITY_RESPONSE: 1,
  FIND_RESULT: 2,
  SELECT_RESULT: 3,
  COUNT_RESULT: 4
};

/**
 * @return {proto.caosdb.entity.v1.RetrieveResponse.RetrieveResponseCase}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.getRetrieveResponseCase = function() {
  return /** @type {proto.caosdb.entity.v1.RetrieveResponse.RetrieveResponseCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.RetrieveResponse.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.RetrieveResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.RetrieveResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.RetrieveResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    entityResponse: (f = msg.getEntityResponse()) && proto.caosdb.entity.v1.EntityResponse.toObject(includeInstance, f),
    findResult: (f = msg.getFindResult()) && proto.caosdb.entity.v1.FindQueryResult.toObject(includeInstance, f),
    selectResult: (f = msg.getSelectResult()) && proto.caosdb.entity.v1.SelectQueryResult.toObject(includeInstance, f),
    countResult: jspb.Message.getFieldWithDefault(msg, 4, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.RetrieveResponse}
 */
proto.caosdb.entity.v1.RetrieveResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.RetrieveResponse;
  return proto.caosdb.entity.v1.RetrieveResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.RetrieveResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.RetrieveResponse}
 */
proto.caosdb.entity.v1.RetrieveResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.EntityResponse;
      reader.readMessage(value,proto.caosdb.entity.v1.EntityResponse.deserializeBinaryFromReader);
      msg.setEntityResponse(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.FindQueryResult;
      reader.readMessage(value,proto.caosdb.entity.v1.FindQueryResult.deserializeBinaryFromReader);
      msg.setFindResult(value);
      break;
    case 3:
      var value = new proto.caosdb.entity.v1.SelectQueryResult;
      reader.readMessage(value,proto.caosdb.entity.v1.SelectQueryResult.deserializeBinaryFromReader);
      msg.setSelectResult(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCountResult(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.RetrieveResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.RetrieveResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.RetrieveResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEntityResponse();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.EntityResponse.serializeBinaryToWriter
    );
  }
  f = message.getFindResult();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.caosdb.entity.v1.FindQueryResult.serializeBinaryToWriter
    );
  }
  f = message.getSelectResult();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.caosdb.entity.v1.SelectQueryResult.serializeBinaryToWriter
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 4));
  if (f != null) {
    writer.writeInt64(
      4,
      f
    );
  }
};


/**
 * optional EntityResponse entity_response = 1;
 * @return {?proto.caosdb.entity.v1.EntityResponse}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.getEntityResponse = function() {
  return /** @type{?proto.caosdb.entity.v1.EntityResponse} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.EntityResponse, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.EntityResponse|undefined} value
 * @return {!proto.caosdb.entity.v1.RetrieveResponse} returns this
*/
proto.caosdb.entity.v1.RetrieveResponse.prototype.setEntityResponse = function(value) {
  return jspb.Message.setOneofWrapperField(this, 1, proto.caosdb.entity.v1.RetrieveResponse.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.RetrieveResponse} returns this
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.clearEntityResponse = function() {
  return this.setEntityResponse(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.hasEntityResponse = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional FindQueryResult find_result = 2;
 * @return {?proto.caosdb.entity.v1.FindQueryResult}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.getFindResult = function() {
  return /** @type{?proto.caosdb.entity.v1.FindQueryResult} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.FindQueryResult, 2));
};


/**
 * @param {?proto.caosdb.entity.v1.FindQueryResult|undefined} value
 * @return {!proto.caosdb.entity.v1.RetrieveResponse} returns this
*/
proto.caosdb.entity.v1.RetrieveResponse.prototype.setFindResult = function(value) {
  return jspb.Message.setOneofWrapperField(this, 2, proto.caosdb.entity.v1.RetrieveResponse.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.RetrieveResponse} returns this
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.clearFindResult = function() {
  return this.setFindResult(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.hasFindResult = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional SelectQueryResult select_result = 3;
 * @return {?proto.caosdb.entity.v1.SelectQueryResult}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.getSelectResult = function() {
  return /** @type{?proto.caosdb.entity.v1.SelectQueryResult} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.SelectQueryResult, 3));
};


/**
 * @param {?proto.caosdb.entity.v1.SelectQueryResult|undefined} value
 * @return {!proto.caosdb.entity.v1.RetrieveResponse} returns this
*/
proto.caosdb.entity.v1.RetrieveResponse.prototype.setSelectResult = function(value) {
  return jspb.Message.setOneofWrapperField(this, 3, proto.caosdb.entity.v1.RetrieveResponse.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.RetrieveResponse} returns this
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.clearSelectResult = function() {
  return this.setSelectResult(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.hasSelectResult = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional int64 count_result = 4;
 * @return {number}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.getCountResult = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.caosdb.entity.v1.RetrieveResponse} returns this
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.setCountResult = function(value) {
  return jspb.Message.setOneofField(this, 4, proto.caosdb.entity.v1.RetrieveResponse.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.caosdb.entity.v1.RetrieveResponse} returns this
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.clearCountResult = function() {
  return jspb.Message.setOneofField(this, 4, proto.caosdb.entity.v1.RetrieveResponse.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.RetrieveResponse.prototype.hasCountResult = function() {
  return jspb.Message.getField(this, 4) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.DeleteRequest.oneofGroups_ = [[1]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.DeleteRequest.WrappedRequestCase = {
  WRAPPED_REQUEST_NOT_SET: 0,
  ID: 1
};

/**
 * @return {proto.caosdb.entity.v1.DeleteRequest.WrappedRequestCase}
 */
proto.caosdb.entity.v1.DeleteRequest.prototype.getWrappedRequestCase = function() {
  return /** @type {proto.caosdb.entity.v1.DeleteRequest.WrappedRequestCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.DeleteRequest.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.DeleteRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.DeleteRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.DeleteRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.DeleteRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.DeleteRequest}
 */
proto.caosdb.entity.v1.DeleteRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.DeleteRequest;
  return proto.caosdb.entity.v1.DeleteRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.DeleteRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.DeleteRequest}
 */
proto.caosdb.entity.v1.DeleteRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.DeleteRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.DeleteRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.DeleteRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.DeleteRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {string} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.DeleteRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.DeleteRequest} returns this
 */
proto.caosdb.entity.v1.DeleteRequest.prototype.setId = function(value) {
  return jspb.Message.setOneofField(this, 1, proto.caosdb.entity.v1.DeleteRequest.oneofGroups_[0], value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.caosdb.entity.v1.DeleteRequest} returns this
 */
proto.caosdb.entity.v1.DeleteRequest.prototype.clearId = function() {
  return jspb.Message.setOneofField(this, 1, proto.caosdb.entity.v1.DeleteRequest.oneofGroups_[0], undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.DeleteRequest.prototype.hasId = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.DeleteResponse.oneofGroups_ = [[1]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.DeleteResponse.WrappedResponseCase = {
  WRAPPED_RESPONSE_NOT_SET: 0,
  ID_RESPONSE: 1
};

/**
 * @return {proto.caosdb.entity.v1.DeleteResponse.WrappedResponseCase}
 */
proto.caosdb.entity.v1.DeleteResponse.prototype.getWrappedResponseCase = function() {
  return /** @type {proto.caosdb.entity.v1.DeleteResponse.WrappedResponseCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.DeleteResponse.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.DeleteResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.DeleteResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.DeleteResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.DeleteResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    idResponse: (f = msg.getIdResponse()) && proto.caosdb.entity.v1.IdResponse.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.DeleteResponse}
 */
proto.caosdb.entity.v1.DeleteResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.DeleteResponse;
  return proto.caosdb.entity.v1.DeleteResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.DeleteResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.DeleteResponse}
 */
proto.caosdb.entity.v1.DeleteResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.IdResponse;
      reader.readMessage(value,proto.caosdb.entity.v1.IdResponse.deserializeBinaryFromReader);
      msg.setIdResponse(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.DeleteResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.DeleteResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.DeleteResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.DeleteResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIdResponse();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.IdResponse.serializeBinaryToWriter
    );
  }
};


/**
 * optional IdResponse id_response = 1;
 * @return {?proto.caosdb.entity.v1.IdResponse}
 */
proto.caosdb.entity.v1.DeleteResponse.prototype.getIdResponse = function() {
  return /** @type{?proto.caosdb.entity.v1.IdResponse} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.IdResponse, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.IdResponse|undefined} value
 * @return {!proto.caosdb.entity.v1.DeleteResponse} returns this
*/
proto.caosdb.entity.v1.DeleteResponse.prototype.setIdResponse = function(value) {
  return jspb.Message.setOneofWrapperField(this, 1, proto.caosdb.entity.v1.DeleteResponse.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.DeleteResponse} returns this
 */
proto.caosdb.entity.v1.DeleteResponse.prototype.clearIdResponse = function() {
  return this.setIdResponse(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.DeleteResponse.prototype.hasIdResponse = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.UpdateRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.UpdateRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.UpdateRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.UpdateRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    entityRequest: (f = msg.getEntityRequest()) && proto.caosdb.entity.v1.EntityRequest.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.UpdateRequest}
 */
proto.caosdb.entity.v1.UpdateRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.UpdateRequest;
  return proto.caosdb.entity.v1.UpdateRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.UpdateRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.UpdateRequest}
 */
proto.caosdb.entity.v1.UpdateRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.EntityRequest;
      reader.readMessage(value,proto.caosdb.entity.v1.EntityRequest.deserializeBinaryFromReader);
      msg.setEntityRequest(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.UpdateRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.UpdateRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.UpdateRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.UpdateRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEntityRequest();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.EntityRequest.serializeBinaryToWriter
    );
  }
};


/**
 * optional EntityRequest entity_request = 1;
 * @return {?proto.caosdb.entity.v1.EntityRequest}
 */
proto.caosdb.entity.v1.UpdateRequest.prototype.getEntityRequest = function() {
  return /** @type{?proto.caosdb.entity.v1.EntityRequest} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.EntityRequest, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.EntityRequest|undefined} value
 * @return {!proto.caosdb.entity.v1.UpdateRequest} returns this
*/
proto.caosdb.entity.v1.UpdateRequest.prototype.setEntityRequest = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.UpdateRequest} returns this
 */
proto.caosdb.entity.v1.UpdateRequest.prototype.clearEntityRequest = function() {
  return this.setEntityRequest(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.UpdateRequest.prototype.hasEntityRequest = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.UpdateResponse.oneofGroups_ = [[1]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.UpdateResponse.WrappedResponseCase = {
  WRAPPED_RESPONSE_NOT_SET: 0,
  ID_RESPONSE: 1
};

/**
 * @return {proto.caosdb.entity.v1.UpdateResponse.WrappedResponseCase}
 */
proto.caosdb.entity.v1.UpdateResponse.prototype.getWrappedResponseCase = function() {
  return /** @type {proto.caosdb.entity.v1.UpdateResponse.WrappedResponseCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.UpdateResponse.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.UpdateResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.UpdateResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.UpdateResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.UpdateResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    idResponse: (f = msg.getIdResponse()) && proto.caosdb.entity.v1.IdResponse.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.UpdateResponse}
 */
proto.caosdb.entity.v1.UpdateResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.UpdateResponse;
  return proto.caosdb.entity.v1.UpdateResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.UpdateResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.UpdateResponse}
 */
proto.caosdb.entity.v1.UpdateResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.IdResponse;
      reader.readMessage(value,proto.caosdb.entity.v1.IdResponse.deserializeBinaryFromReader);
      msg.setIdResponse(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.UpdateResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.UpdateResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.UpdateResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.UpdateResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIdResponse();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.IdResponse.serializeBinaryToWriter
    );
  }
};


/**
 * optional IdResponse id_response = 1;
 * @return {?proto.caosdb.entity.v1.IdResponse}
 */
proto.caosdb.entity.v1.UpdateResponse.prototype.getIdResponse = function() {
  return /** @type{?proto.caosdb.entity.v1.IdResponse} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.IdResponse, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.IdResponse|undefined} value
 * @return {!proto.caosdb.entity.v1.UpdateResponse} returns this
*/
proto.caosdb.entity.v1.UpdateResponse.prototype.setIdResponse = function(value) {
  return jspb.Message.setOneofWrapperField(this, 1, proto.caosdb.entity.v1.UpdateResponse.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.UpdateResponse} returns this
 */
proto.caosdb.entity.v1.UpdateResponse.prototype.clearIdResponse = function() {
  return this.setIdResponse(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.UpdateResponse.prototype.hasIdResponse = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.InsertRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.InsertRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.InsertRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.InsertRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    entityRequest: (f = msg.getEntityRequest()) && proto.caosdb.entity.v1.EntityRequest.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.InsertRequest}
 */
proto.caosdb.entity.v1.InsertRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.InsertRequest;
  return proto.caosdb.entity.v1.InsertRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.InsertRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.InsertRequest}
 */
proto.caosdb.entity.v1.InsertRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.EntityRequest;
      reader.readMessage(value,proto.caosdb.entity.v1.EntityRequest.deserializeBinaryFromReader);
      msg.setEntityRequest(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.InsertRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.InsertRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.InsertRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.InsertRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEntityRequest();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.EntityRequest.serializeBinaryToWriter
    );
  }
};


/**
 * optional EntityRequest entity_request = 1;
 * @return {?proto.caosdb.entity.v1.EntityRequest}
 */
proto.caosdb.entity.v1.InsertRequest.prototype.getEntityRequest = function() {
  return /** @type{?proto.caosdb.entity.v1.EntityRequest} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.EntityRequest, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.EntityRequest|undefined} value
 * @return {!proto.caosdb.entity.v1.InsertRequest} returns this
*/
proto.caosdb.entity.v1.InsertRequest.prototype.setEntityRequest = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.InsertRequest} returns this
 */
proto.caosdb.entity.v1.InsertRequest.prototype.clearEntityRequest = function() {
  return this.setEntityRequest(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.InsertRequest.prototype.hasEntityRequest = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.InsertResponse.oneofGroups_ = [[1]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.InsertResponse.WrappedResponseCase = {
  WRAPPED_RESPONSE_NOT_SET: 0,
  ID_RESPONSE: 1
};

/**
 * @return {proto.caosdb.entity.v1.InsertResponse.WrappedResponseCase}
 */
proto.caosdb.entity.v1.InsertResponse.prototype.getWrappedResponseCase = function() {
  return /** @type {proto.caosdb.entity.v1.InsertResponse.WrappedResponseCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.InsertResponse.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.InsertResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.InsertResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.InsertResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.InsertResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    idResponse: (f = msg.getIdResponse()) && proto.caosdb.entity.v1.IdResponse.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.InsertResponse}
 */
proto.caosdb.entity.v1.InsertResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.InsertResponse;
  return proto.caosdb.entity.v1.InsertResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.InsertResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.InsertResponse}
 */
proto.caosdb.entity.v1.InsertResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.IdResponse;
      reader.readMessage(value,proto.caosdb.entity.v1.IdResponse.deserializeBinaryFromReader);
      msg.setIdResponse(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.InsertResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.InsertResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.InsertResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.InsertResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIdResponse();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.IdResponse.serializeBinaryToWriter
    );
  }
};


/**
 * optional IdResponse id_response = 1;
 * @return {?proto.caosdb.entity.v1.IdResponse}
 */
proto.caosdb.entity.v1.InsertResponse.prototype.getIdResponse = function() {
  return /** @type{?proto.caosdb.entity.v1.IdResponse} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.IdResponse, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.IdResponse|undefined} value
 * @return {!proto.caosdb.entity.v1.InsertResponse} returns this
*/
proto.caosdb.entity.v1.InsertResponse.prototype.setIdResponse = function(value) {
  return jspb.Message.setOneofWrapperField(this, 1, proto.caosdb.entity.v1.InsertResponse.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.InsertResponse} returns this
 */
proto.caosdb.entity.v1.InsertResponse.prototype.clearIdResponse = function() {
  return this.setIdResponse(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.InsertResponse.prototype.hasIdResponse = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.TransactionRequest.oneofGroups_ = [[1,2,3,4]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.TransactionRequest.WrappedRequestsCase = {
  WRAPPED_REQUESTS_NOT_SET: 0,
  RETRIEVE_REQUEST: 1,
  UPDATE_REQUEST: 2,
  INSERT_REQUEST: 3,
  DELETE_REQUEST: 4
};

/**
 * @return {proto.caosdb.entity.v1.TransactionRequest.WrappedRequestsCase}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.getWrappedRequestsCase = function() {
  return /** @type {proto.caosdb.entity.v1.TransactionRequest.WrappedRequestsCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.TransactionRequest.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.TransactionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.TransactionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.TransactionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    retrieveRequest: (f = msg.getRetrieveRequest()) && proto.caosdb.entity.v1.RetrieveRequest.toObject(includeInstance, f),
    updateRequest: (f = msg.getUpdateRequest()) && proto.caosdb.entity.v1.UpdateRequest.toObject(includeInstance, f),
    insertRequest: (f = msg.getInsertRequest()) && proto.caosdb.entity.v1.InsertRequest.toObject(includeInstance, f),
    deleteRequest: (f = msg.getDeleteRequest()) && proto.caosdb.entity.v1.DeleteRequest.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.TransactionRequest}
 */
proto.caosdb.entity.v1.TransactionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.TransactionRequest;
  return proto.caosdb.entity.v1.TransactionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.TransactionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.TransactionRequest}
 */
proto.caosdb.entity.v1.TransactionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.RetrieveRequest;
      reader.readMessage(value,proto.caosdb.entity.v1.RetrieveRequest.deserializeBinaryFromReader);
      msg.setRetrieveRequest(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.UpdateRequest;
      reader.readMessage(value,proto.caosdb.entity.v1.UpdateRequest.deserializeBinaryFromReader);
      msg.setUpdateRequest(value);
      break;
    case 3:
      var value = new proto.caosdb.entity.v1.InsertRequest;
      reader.readMessage(value,proto.caosdb.entity.v1.InsertRequest.deserializeBinaryFromReader);
      msg.setInsertRequest(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.DeleteRequest;
      reader.readMessage(value,proto.caosdb.entity.v1.DeleteRequest.deserializeBinaryFromReader);
      msg.setDeleteRequest(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.TransactionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.TransactionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.TransactionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRetrieveRequest();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.RetrieveRequest.serializeBinaryToWriter
    );
  }
  f = message.getUpdateRequest();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.caosdb.entity.v1.UpdateRequest.serializeBinaryToWriter
    );
  }
  f = message.getInsertRequest();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.caosdb.entity.v1.InsertRequest.serializeBinaryToWriter
    );
  }
  f = message.getDeleteRequest();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.caosdb.entity.v1.DeleteRequest.serializeBinaryToWriter
    );
  }
};


/**
 * optional RetrieveRequest retrieve_request = 1;
 * @return {?proto.caosdb.entity.v1.RetrieveRequest}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.getRetrieveRequest = function() {
  return /** @type{?proto.caosdb.entity.v1.RetrieveRequest} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.RetrieveRequest, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.RetrieveRequest|undefined} value
 * @return {!proto.caosdb.entity.v1.TransactionRequest} returns this
*/
proto.caosdb.entity.v1.TransactionRequest.prototype.setRetrieveRequest = function(value) {
  return jspb.Message.setOneofWrapperField(this, 1, proto.caosdb.entity.v1.TransactionRequest.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.TransactionRequest} returns this
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.clearRetrieveRequest = function() {
  return this.setRetrieveRequest(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.hasRetrieveRequest = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional UpdateRequest update_request = 2;
 * @return {?proto.caosdb.entity.v1.UpdateRequest}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.getUpdateRequest = function() {
  return /** @type{?proto.caosdb.entity.v1.UpdateRequest} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.UpdateRequest, 2));
};


/**
 * @param {?proto.caosdb.entity.v1.UpdateRequest|undefined} value
 * @return {!proto.caosdb.entity.v1.TransactionRequest} returns this
*/
proto.caosdb.entity.v1.TransactionRequest.prototype.setUpdateRequest = function(value) {
  return jspb.Message.setOneofWrapperField(this, 2, proto.caosdb.entity.v1.TransactionRequest.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.TransactionRequest} returns this
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.clearUpdateRequest = function() {
  return this.setUpdateRequest(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.hasUpdateRequest = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional InsertRequest insert_request = 3;
 * @return {?proto.caosdb.entity.v1.InsertRequest}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.getInsertRequest = function() {
  return /** @type{?proto.caosdb.entity.v1.InsertRequest} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.InsertRequest, 3));
};


/**
 * @param {?proto.caosdb.entity.v1.InsertRequest|undefined} value
 * @return {!proto.caosdb.entity.v1.TransactionRequest} returns this
*/
proto.caosdb.entity.v1.TransactionRequest.prototype.setInsertRequest = function(value) {
  return jspb.Message.setOneofWrapperField(this, 3, proto.caosdb.entity.v1.TransactionRequest.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.TransactionRequest} returns this
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.clearInsertRequest = function() {
  return this.setInsertRequest(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.hasInsertRequest = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional DeleteRequest delete_request = 4;
 * @return {?proto.caosdb.entity.v1.DeleteRequest}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.getDeleteRequest = function() {
  return /** @type{?proto.caosdb.entity.v1.DeleteRequest} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.DeleteRequest, 4));
};


/**
 * @param {?proto.caosdb.entity.v1.DeleteRequest|undefined} value
 * @return {!proto.caosdb.entity.v1.TransactionRequest} returns this
*/
proto.caosdb.entity.v1.TransactionRequest.prototype.setDeleteRequest = function(value) {
  return jspb.Message.setOneofWrapperField(this, 4, proto.caosdb.entity.v1.TransactionRequest.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.TransactionRequest} returns this
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.clearDeleteRequest = function() {
  return this.setDeleteRequest(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.TransactionRequest.prototype.hasDeleteRequest = function() {
  return jspb.Message.getField(this, 4) != null;
};



/**
 * Oneof group definitions for this message. Each group defines the field
 * numbers belonging to that group. When of these fields' value is set, all
 * other fields in the group are cleared. During deserialization, if multiple
 * fields are encountered for a group, only the last value seen will be kept.
 * @private {!Array<!Array<number>>}
 * @const
 */
proto.caosdb.entity.v1.TransactionResponse.oneofGroups_ = [[1,2,3,4]];

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.TransactionResponse.TransactionResponseCase = {
  TRANSACTION_RESPONSE_NOT_SET: 0,
  DELETE_RESPONSE: 1,
  UPDATE_RESPONSE: 2,
  RETRIEVE_RESPONSE: 3,
  INSERT_RESPONSE: 4
};

/**
 * @return {proto.caosdb.entity.v1.TransactionResponse.TransactionResponseCase}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.getTransactionResponseCase = function() {
  return /** @type {proto.caosdb.entity.v1.TransactionResponse.TransactionResponseCase} */(jspb.Message.computeOneofCase(this, proto.caosdb.entity.v1.TransactionResponse.oneofGroups_[0]));
};



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.TransactionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.TransactionResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.TransactionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    deleteResponse: (f = msg.getDeleteResponse()) && proto.caosdb.entity.v1.DeleteResponse.toObject(includeInstance, f),
    updateResponse: (f = msg.getUpdateResponse()) && proto.caosdb.entity.v1.UpdateResponse.toObject(includeInstance, f),
    retrieveResponse: (f = msg.getRetrieveResponse()) && proto.caosdb.entity.v1.RetrieveResponse.toObject(includeInstance, f),
    insertResponse: (f = msg.getInsertResponse()) && proto.caosdb.entity.v1.InsertResponse.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.TransactionResponse}
 */
proto.caosdb.entity.v1.TransactionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.TransactionResponse;
  return proto.caosdb.entity.v1.TransactionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.TransactionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.TransactionResponse}
 */
proto.caosdb.entity.v1.TransactionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.DeleteResponse;
      reader.readMessage(value,proto.caosdb.entity.v1.DeleteResponse.deserializeBinaryFromReader);
      msg.setDeleteResponse(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.UpdateResponse;
      reader.readMessage(value,proto.caosdb.entity.v1.UpdateResponse.deserializeBinaryFromReader);
      msg.setUpdateResponse(value);
      break;
    case 3:
      var value = new proto.caosdb.entity.v1.RetrieveResponse;
      reader.readMessage(value,proto.caosdb.entity.v1.RetrieveResponse.deserializeBinaryFromReader);
      msg.setRetrieveResponse(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.InsertResponse;
      reader.readMessage(value,proto.caosdb.entity.v1.InsertResponse.deserializeBinaryFromReader);
      msg.setInsertResponse(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.TransactionResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.TransactionResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.TransactionResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getDeleteResponse();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.DeleteResponse.serializeBinaryToWriter
    );
  }
  f = message.getUpdateResponse();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.caosdb.entity.v1.UpdateResponse.serializeBinaryToWriter
    );
  }
  f = message.getRetrieveResponse();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.caosdb.entity.v1.RetrieveResponse.serializeBinaryToWriter
    );
  }
  f = message.getInsertResponse();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.caosdb.entity.v1.InsertResponse.serializeBinaryToWriter
    );
  }
};


/**
 * optional DeleteResponse delete_response = 1;
 * @return {?proto.caosdb.entity.v1.DeleteResponse}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.getDeleteResponse = function() {
  return /** @type{?proto.caosdb.entity.v1.DeleteResponse} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.DeleteResponse, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.DeleteResponse|undefined} value
 * @return {!proto.caosdb.entity.v1.TransactionResponse} returns this
*/
proto.caosdb.entity.v1.TransactionResponse.prototype.setDeleteResponse = function(value) {
  return jspb.Message.setOneofWrapperField(this, 1, proto.caosdb.entity.v1.TransactionResponse.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.TransactionResponse} returns this
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.clearDeleteResponse = function() {
  return this.setDeleteResponse(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.hasDeleteResponse = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional UpdateResponse update_response = 2;
 * @return {?proto.caosdb.entity.v1.UpdateResponse}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.getUpdateResponse = function() {
  return /** @type{?proto.caosdb.entity.v1.UpdateResponse} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.UpdateResponse, 2));
};


/**
 * @param {?proto.caosdb.entity.v1.UpdateResponse|undefined} value
 * @return {!proto.caosdb.entity.v1.TransactionResponse} returns this
*/
proto.caosdb.entity.v1.TransactionResponse.prototype.setUpdateResponse = function(value) {
  return jspb.Message.setOneofWrapperField(this, 2, proto.caosdb.entity.v1.TransactionResponse.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.TransactionResponse} returns this
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.clearUpdateResponse = function() {
  return this.setUpdateResponse(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.hasUpdateResponse = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional RetrieveResponse retrieve_response = 3;
 * @return {?proto.caosdb.entity.v1.RetrieveResponse}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.getRetrieveResponse = function() {
  return /** @type{?proto.caosdb.entity.v1.RetrieveResponse} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.RetrieveResponse, 3));
};


/**
 * @param {?proto.caosdb.entity.v1.RetrieveResponse|undefined} value
 * @return {!proto.caosdb.entity.v1.TransactionResponse} returns this
*/
proto.caosdb.entity.v1.TransactionResponse.prototype.setRetrieveResponse = function(value) {
  return jspb.Message.setOneofWrapperField(this, 3, proto.caosdb.entity.v1.TransactionResponse.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.TransactionResponse} returns this
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.clearRetrieveResponse = function() {
  return this.setRetrieveResponse(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.hasRetrieveResponse = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional InsertResponse insert_response = 4;
 * @return {?proto.caosdb.entity.v1.InsertResponse}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.getInsertResponse = function() {
  return /** @type{?proto.caosdb.entity.v1.InsertResponse} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.InsertResponse, 4));
};


/**
 * @param {?proto.caosdb.entity.v1.InsertResponse|undefined} value
 * @return {!proto.caosdb.entity.v1.TransactionResponse} returns this
*/
proto.caosdb.entity.v1.TransactionResponse.prototype.setInsertResponse = function(value) {
  return jspb.Message.setOneofWrapperField(this, 4, proto.caosdb.entity.v1.TransactionResponse.oneofGroups_[0], value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.TransactionResponse} returns this
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.clearInsertResponse = function() {
  return this.setInsertResponse(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.TransactionResponse.prototype.hasInsertResponse = function() {
  return jspb.Message.getField(this, 4) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.MultiTransactionRequest.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.MultiTransactionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.MultiTransactionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.MultiTransactionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiTransactionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    requestsList: jspb.Message.toObjectList(msg.getRequestsList(),
    proto.caosdb.entity.v1.TransactionRequest.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.MultiTransactionRequest}
 */
proto.caosdb.entity.v1.MultiTransactionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.MultiTransactionRequest;
  return proto.caosdb.entity.v1.MultiTransactionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.MultiTransactionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.MultiTransactionRequest}
 */
proto.caosdb.entity.v1.MultiTransactionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.TransactionRequest;
      reader.readMessage(value,proto.caosdb.entity.v1.TransactionRequest.deserializeBinaryFromReader);
      msg.addRequests(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.MultiTransactionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.MultiTransactionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.MultiTransactionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiTransactionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRequestsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.caosdb.entity.v1.TransactionRequest.serializeBinaryToWriter
    );
  }
};


/**
 * repeated TransactionRequest requests = 1;
 * @return {!Array<!proto.caosdb.entity.v1.TransactionRequest>}
 */
proto.caosdb.entity.v1.MultiTransactionRequest.prototype.getRequestsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.TransactionRequest>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.TransactionRequest, 1));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.TransactionRequest>} value
 * @return {!proto.caosdb.entity.v1.MultiTransactionRequest} returns this
*/
proto.caosdb.entity.v1.MultiTransactionRequest.prototype.setRequestsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.caosdb.entity.v1.TransactionRequest=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.TransactionRequest}
 */
proto.caosdb.entity.v1.MultiTransactionRequest.prototype.addRequests = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.caosdb.entity.v1.TransactionRequest, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.MultiTransactionRequest} returns this
 */
proto.caosdb.entity.v1.MultiTransactionRequest.prototype.clearRequestsList = function() {
  return this.setRequestsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.MultiTransactionResponse.repeatedFields_ = [1,2,3,4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.MultiTransactionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.MultiTransactionResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiTransactionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    responsesList: jspb.Message.toObjectList(msg.getResponsesList(),
    proto.caosdb.entity.v1.TransactionResponse.toObject, includeInstance),
    transactionErrorsList: jspb.Message.toObjectList(msg.getTransactionErrorsList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance),
    transactionWarningsList: jspb.Message.toObjectList(msg.getTransactionWarningsList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance),
    transactionInfosList: jspb.Message.toObjectList(msg.getTransactionInfosList(),
    proto.caosdb.entity.v1.Message.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.MultiTransactionResponse}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.MultiTransactionResponse;
  return proto.caosdb.entity.v1.MultiTransactionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.MultiTransactionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.MultiTransactionResponse}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.TransactionResponse;
      reader.readMessage(value,proto.caosdb.entity.v1.TransactionResponse.deserializeBinaryFromReader);
      msg.addResponses(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addTransactionErrors(value);
      break;
    case 3:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addTransactionWarnings(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.Message;
      reader.readMessage(value,proto.caosdb.entity.v1.Message.deserializeBinaryFromReader);
      msg.addTransactionInfos(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.MultiTransactionResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.MultiTransactionResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiTransactionResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getResponsesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.caosdb.entity.v1.TransactionResponse.serializeBinaryToWriter
    );
  }
  f = message.getTransactionErrorsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
  f = message.getTransactionWarningsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
  f = message.getTransactionInfosList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.caosdb.entity.v1.Message.serializeBinaryToWriter
    );
  }
};


/**
 * repeated TransactionResponse responses = 1;
 * @return {!Array<!proto.caosdb.entity.v1.TransactionResponse>}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.getResponsesList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.TransactionResponse>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.TransactionResponse, 1));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.TransactionResponse>} value
 * @return {!proto.caosdb.entity.v1.MultiTransactionResponse} returns this
*/
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.setResponsesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.caosdb.entity.v1.TransactionResponse=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.TransactionResponse}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.addResponses = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.caosdb.entity.v1.TransactionResponse, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.MultiTransactionResponse} returns this
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.clearResponsesList = function() {
  return this.setResponsesList([]);
};


/**
 * repeated Message transaction_errors = 2;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.getTransactionErrorsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 2));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.MultiTransactionResponse} returns this
*/
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.setTransactionErrorsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.addTransactionErrors = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.MultiTransactionResponse} returns this
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.clearTransactionErrorsList = function() {
  return this.setTransactionErrorsList([]);
};


/**
 * repeated Message transaction_warnings = 3;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.getTransactionWarningsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 3));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.MultiTransactionResponse} returns this
*/
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.setTransactionWarningsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.addTransactionWarnings = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.MultiTransactionResponse} returns this
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.clearTransactionWarningsList = function() {
  return this.setTransactionWarningsList([]);
};


/**
 * repeated Message transaction_infos = 4;
 * @return {!Array<!proto.caosdb.entity.v1.Message>}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.getTransactionInfosList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.Message>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.Message, 4));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.Message>} value
 * @return {!proto.caosdb.entity.v1.MultiTransactionResponse} returns this
*/
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.setTransactionInfosList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.caosdb.entity.v1.Message=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.Message}
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.addTransactionInfos = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.caosdb.entity.v1.Message, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.MultiTransactionResponse} returns this
 */
proto.caosdb.entity.v1.MultiTransactionResponse.prototype.clearTransactionInfosList = function() {
  return this.setTransactionInfosList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    idList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest;
  return proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.addId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIdList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      1,
      f
    );
  }
};


/**
 * repeated string id = 1;
 * @return {!Array<string>}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.prototype.getIdList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest} returns this
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.prototype.setIdList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest} returns this
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.prototype.addId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest} returns this
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLRequest.prototype.clearIdList = function() {
  return this.setIdList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    aclsList: jspb.Message.toObjectList(msg.getAclsList(),
    proto.caosdb.entity.v1.EntityACL.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse;
  return proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.EntityACL;
      reader.readMessage(value,proto.caosdb.entity.v1.EntityACL.deserializeBinaryFromReader);
      msg.addAcls(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAclsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.caosdb.entity.v1.EntityACL.serializeBinaryToWriter
    );
  }
};


/**
 * repeated EntityACL acls = 1;
 * @return {!Array<!proto.caosdb.entity.v1.EntityACL>}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.prototype.getAclsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.EntityACL>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.EntityACL, 1));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.EntityACL>} value
 * @return {!proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse} returns this
*/
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.prototype.setAclsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.caosdb.entity.v1.EntityACL=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.EntityACL}
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.prototype.addAcls = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.caosdb.entity.v1.EntityACL, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse} returns this
 */
proto.caosdb.entity.v1.MultiRetrieveEntityACLResponse.prototype.clearAclsList = function() {
  return this.setAclsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.MultiUpdateEntityACLRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    aclsList: jspb.Message.toObjectList(msg.getAclsList(),
    proto.caosdb.entity.v1.EntityACL.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.MultiUpdateEntityACLRequest}
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.MultiUpdateEntityACLRequest;
  return proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.MultiUpdateEntityACLRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.MultiUpdateEntityACLRequest}
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.EntityACL;
      reader.readMessage(value,proto.caosdb.entity.v1.EntityACL.deserializeBinaryFromReader);
      msg.addAcls(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.MultiUpdateEntityACLRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAclsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.caosdb.entity.v1.EntityACL.serializeBinaryToWriter
    );
  }
};


/**
 * repeated EntityACL acls = 1;
 * @return {!Array<!proto.caosdb.entity.v1.EntityACL>}
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.prototype.getAclsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.EntityACL>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.EntityACL, 1));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.EntityACL>} value
 * @return {!proto.caosdb.entity.v1.MultiUpdateEntityACLRequest} returns this
*/
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.prototype.setAclsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.caosdb.entity.v1.EntityACL=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.EntityACL}
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.prototype.addAcls = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.caosdb.entity.v1.EntityACL, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.MultiUpdateEntityACLRequest} returns this
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLRequest.prototype.clearAclsList = function() {
  return this.setAclsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.MultiUpdateEntityACLResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.MultiUpdateEntityACLResponse}
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.MultiUpdateEntityACLResponse;
  return proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.MultiUpdateEntityACLResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.MultiUpdateEntityACLResponse}
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.MultiUpdateEntityACLResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.MultiUpdateEntityACLResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.EntityACL.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.EntityACL.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.EntityACL.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.EntityACL} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.EntityACL.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    rulesList: jspb.Message.toObjectList(msg.getRulesList(),
    proto.caosdb.entity.v1.EntityPermissionRule.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.EntityACL}
 */
proto.caosdb.entity.v1.EntityACL.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.EntityACL;
  return proto.caosdb.entity.v1.EntityACL.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.EntityACL} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.EntityACL}
 */
proto.caosdb.entity.v1.EntityACL.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.EntityPermissionRule;
      reader.readMessage(value,proto.caosdb.entity.v1.EntityPermissionRule.deserializeBinaryFromReader);
      msg.addRules(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.EntityACL.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.EntityACL.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.EntityACL} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.EntityACL.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getRulesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.caosdb.entity.v1.EntityPermissionRule.serializeBinaryToWriter
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.EntityACL.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.EntityACL} returns this
 */
proto.caosdb.entity.v1.EntityACL.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * repeated EntityPermissionRule rules = 2;
 * @return {!Array<!proto.caosdb.entity.v1.EntityPermissionRule>}
 */
proto.caosdb.entity.v1.EntityACL.prototype.getRulesList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.EntityPermissionRule>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.EntityPermissionRule, 2));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.EntityPermissionRule>} value
 * @return {!proto.caosdb.entity.v1.EntityACL} returns this
*/
proto.caosdb.entity.v1.EntityACL.prototype.setRulesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.caosdb.entity.v1.EntityPermissionRule=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.EntityPermissionRule}
 */
proto.caosdb.entity.v1.EntityACL.prototype.addRules = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.caosdb.entity.v1.EntityPermissionRule, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.EntityACL} returns this
 */
proto.caosdb.entity.v1.EntityACL.prototype.clearRulesList = function() {
  return this.setRulesList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.caosdb.entity.v1.EntityPermissionRule.repeatedFields_ = [4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.EntityPermissionRule.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.EntityPermissionRule} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.EntityPermissionRule.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: jspb.Message.getFieldWithDefault(msg, 1, ""),
    priority: jspb.Message.getBooleanFieldWithDefault(msg, 2, false),
    grant: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    permissionsList: jspb.Message.toObjectList(msg.getPermissionsList(),
    proto.caosdb.entity.v1.EntityPermission.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.EntityPermissionRule}
 */
proto.caosdb.entity.v1.EntityPermissionRule.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.EntityPermissionRule;
  return proto.caosdb.entity.v1.EntityPermissionRule.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.EntityPermissionRule} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.EntityPermissionRule}
 */
proto.caosdb.entity.v1.EntityPermissionRule.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setRole(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setPriority(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setGrant(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.EntityPermission;
      reader.readMessage(value,proto.caosdb.entity.v1.EntityPermission.deserializeBinaryFromReader);
      msg.addPermissions(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.EntityPermissionRule.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.EntityPermissionRule} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.EntityPermissionRule.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPriority();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
  f = message.getGrant();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
  f = message.getPermissionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.caosdb.entity.v1.EntityPermission.serializeBinaryToWriter
    );
  }
};


/**
 * optional string role = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.getRole = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.EntityPermissionRule} returns this
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.setRole = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional bool priority = 2;
 * @return {boolean}
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.getPriority = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.caosdb.entity.v1.EntityPermissionRule} returns this
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.setPriority = function(value) {
  return jspb.Message.setProto3BooleanField(this, 2, value);
};


/**
 * optional bool grant = 3;
 * @return {boolean}
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.getGrant = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.caosdb.entity.v1.EntityPermissionRule} returns this
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.setGrant = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};


/**
 * repeated EntityPermission permissions = 4;
 * @return {!Array<!proto.caosdb.entity.v1.EntityPermission>}
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.getPermissionsList = function() {
  return /** @type{!Array<!proto.caosdb.entity.v1.EntityPermission>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.caosdb.entity.v1.EntityPermission, 4));
};


/**
 * @param {!Array<!proto.caosdb.entity.v1.EntityPermission>} value
 * @return {!proto.caosdb.entity.v1.EntityPermissionRule} returns this
*/
proto.caosdb.entity.v1.EntityPermissionRule.prototype.setPermissionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.caosdb.entity.v1.EntityPermission=} opt_value
 * @param {number=} opt_index
 * @return {!proto.caosdb.entity.v1.EntityPermission}
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.addPermissions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.caosdb.entity.v1.EntityPermission, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.caosdb.entity.v1.EntityPermissionRule} returns this
 */
proto.caosdb.entity.v1.EntityPermissionRule.prototype.clearPermissionsList = function() {
  return this.setPermissionsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.EntityPermission.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.EntityPermission.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.EntityPermission} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.EntityPermission.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.EntityPermission}
 */
proto.caosdb.entity.v1.EntityPermission.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.EntityPermission;
  return proto.caosdb.entity.v1.EntityPermission.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.EntityPermission} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.EntityPermission}
 */
proto.caosdb.entity.v1.EntityPermission.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.EntityPermission.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.EntityPermission.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.EntityPermission} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.EntityPermission.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.EntityPermission.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.EntityPermission} returns this
 */
proto.caosdb.entity.v1.EntityPermission.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.FileChunk.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.FileChunk.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.FileChunk} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileChunk.toObject = function(includeInstance, msg) {
  var f, obj = {
    fileTransmissionId: (f = msg.getFileTransmissionId()) && proto.caosdb.entity.v1.FileTransmissionId.toObject(includeInstance, f),
    data: msg.getData_asB64()
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.FileChunk}
 */
proto.caosdb.entity.v1.FileChunk.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.FileChunk;
  return proto.caosdb.entity.v1.FileChunk.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.FileChunk} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.FileChunk}
 */
proto.caosdb.entity.v1.FileChunk.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.FileTransmissionId;
      reader.readMessage(value,proto.caosdb.entity.v1.FileTransmissionId.deserializeBinaryFromReader);
      msg.setFileTransmissionId(value);
      break;
    case 2:
      var value = /** @type {!Uint8Array} */ (reader.readBytes());
      msg.setData(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.FileChunk.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.FileChunk.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.FileChunk} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileChunk.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFileTransmissionId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.FileTransmissionId.serializeBinaryToWriter
    );
  }
  f = message.getData_asU8();
  if (f.length > 0) {
    writer.writeBytes(
      2,
      f
    );
  }
};


/**
 * optional FileTransmissionId file_transmission_id = 1;
 * @return {?proto.caosdb.entity.v1.FileTransmissionId}
 */
proto.caosdb.entity.v1.FileChunk.prototype.getFileTransmissionId = function() {
  return /** @type{?proto.caosdb.entity.v1.FileTransmissionId} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.FileTransmissionId, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.FileTransmissionId|undefined} value
 * @return {!proto.caosdb.entity.v1.FileChunk} returns this
*/
proto.caosdb.entity.v1.FileChunk.prototype.setFileTransmissionId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.FileChunk} returns this
 */
proto.caosdb.entity.v1.FileChunk.prototype.clearFileTransmissionId = function() {
  return this.setFileTransmissionId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.FileChunk.prototype.hasFileTransmissionId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional bytes data = 2;
 * @return {string}
 */
proto.caosdb.entity.v1.FileChunk.prototype.getData = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * optional bytes data = 2;
 * This is a type-conversion wrapper around `getData()`
 * @return {string}
 */
proto.caosdb.entity.v1.FileChunk.prototype.getData_asB64 = function() {
  return /** @type {string} */ (jspb.Message.bytesAsB64(
      this.getData()));
};


/**
 * optional bytes data = 2;
 * Note that Uint8Array is not supported on all browsers.
 * @see http://caniuse.com/Uint8Array
 * This is a type-conversion wrapper around `getData()`
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.FileChunk.prototype.getData_asU8 = function() {
  return /** @type {!Uint8Array} */ (jspb.Message.bytesAsU8(
      this.getData()));
};


/**
 * @param {!(string|Uint8Array)} value
 * @return {!proto.caosdb.entity.v1.FileChunk} returns this
 */
proto.caosdb.entity.v1.FileChunk.prototype.setData = function(value) {
  return jspb.Message.setProto3BytesField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.FileTransmissionId.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.FileTransmissionId.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.FileTransmissionId} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileTransmissionId.toObject = function(includeInstance, msg) {
  var f, obj = {
    registrationId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    fileId: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.FileTransmissionId}
 */
proto.caosdb.entity.v1.FileTransmissionId.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.FileTransmissionId;
  return proto.caosdb.entity.v1.FileTransmissionId.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.FileTransmissionId} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.FileTransmissionId}
 */
proto.caosdb.entity.v1.FileTransmissionId.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setRegistrationId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setFileId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.FileTransmissionId.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.FileTransmissionId.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.FileTransmissionId} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileTransmissionId.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRegistrationId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getFileId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string registration_id = 1;
 * @return {string}
 */
proto.caosdb.entity.v1.FileTransmissionId.prototype.getRegistrationId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.FileTransmissionId} returns this
 */
proto.caosdb.entity.v1.FileTransmissionId.prototype.setRegistrationId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string file_id = 2;
 * @return {string}
 */
proto.caosdb.entity.v1.FileTransmissionId.prototype.getFileId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.FileTransmissionId} returns this
 */
proto.caosdb.entity.v1.FileTransmissionId.prototype.setFileId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.FileTransmissionSettings.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.FileTransmissionSettings.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.FileTransmissionSettings} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileTransmissionSettings.toObject = function(includeInstance, msg) {
  var f, obj = {
    maxChunkSize: jspb.Message.getFieldWithDefault(msg, 1, 0),
    maxFileSize: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.FileTransmissionSettings}
 */
proto.caosdb.entity.v1.FileTransmissionSettings.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.FileTransmissionSettings;
  return proto.caosdb.entity.v1.FileTransmissionSettings.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.FileTransmissionSettings} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.FileTransmissionSettings}
 */
proto.caosdb.entity.v1.FileTransmissionSettings.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setMaxChunkSize(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setMaxFileSize(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.FileTransmissionSettings.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.FileTransmissionSettings.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.FileTransmissionSettings} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileTransmissionSettings.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getMaxChunkSize();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getMaxFileSize();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
};


/**
 * optional int64 max_chunk_size = 1;
 * @return {number}
 */
proto.caosdb.entity.v1.FileTransmissionSettings.prototype.getMaxChunkSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.caosdb.entity.v1.FileTransmissionSettings} returns this
 */
proto.caosdb.entity.v1.FileTransmissionSettings.prototype.setMaxChunkSize = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int64 max_file_size = 2;
 * @return {number}
 */
proto.caosdb.entity.v1.FileTransmissionSettings.prototype.getMaxFileSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.caosdb.entity.v1.FileTransmissionSettings} returns this
 */
proto.caosdb.entity.v1.FileTransmissionSettings.prototype.setMaxFileSize = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.RegisterFileUploadRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.RegisterFileUploadRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.RegisterFileUploadRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.RegisterFileUploadRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.RegisterFileUploadRequest}
 */
proto.caosdb.entity.v1.RegisterFileUploadRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.RegisterFileUploadRequest;
  return proto.caosdb.entity.v1.RegisterFileUploadRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.RegisterFileUploadRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.RegisterFileUploadRequest}
 */
proto.caosdb.entity.v1.RegisterFileUploadRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.RegisterFileUploadRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.RegisterFileUploadRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.RegisterFileUploadRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.RegisterFileUploadRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.RegisterFileUploadResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.RegisterFileUploadResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: jspb.Message.getFieldWithDefault(msg, 1, 0),
    registrationId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    uploadSettings: (f = msg.getUploadSettings()) && proto.caosdb.entity.v1.FileTransmissionSettings.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.RegisterFileUploadResponse}
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.RegisterFileUploadResponse;
  return proto.caosdb.entity.v1.RegisterFileUploadResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.RegisterFileUploadResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.RegisterFileUploadResponse}
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.caosdb.entity.v1.RegistrationStatus} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setRegistrationId(value);
      break;
    case 4:
      var value = new proto.caosdb.entity.v1.FileTransmissionSettings;
      reader.readMessage(value,proto.caosdb.entity.v1.FileTransmissionSettings.deserializeBinaryFromReader);
      msg.setUploadSettings(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.RegisterFileUploadResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.RegisterFileUploadResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getRegistrationId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getUploadSettings();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.caosdb.entity.v1.FileTransmissionSettings.serializeBinaryToWriter
    );
  }
};


/**
 * optional RegistrationStatus status = 1;
 * @return {!proto.caosdb.entity.v1.RegistrationStatus}
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.prototype.getStatus = function() {
  return /** @type {!proto.caosdb.entity.v1.RegistrationStatus} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.caosdb.entity.v1.RegistrationStatus} value
 * @return {!proto.caosdb.entity.v1.RegisterFileUploadResponse} returns this
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.prototype.setStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string registration_id = 2;
 * @return {string}
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.prototype.getRegistrationId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.caosdb.entity.v1.RegisterFileUploadResponse} returns this
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.prototype.setRegistrationId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional FileTransmissionSettings upload_settings = 4;
 * @return {?proto.caosdb.entity.v1.FileTransmissionSettings}
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.prototype.getUploadSettings = function() {
  return /** @type{?proto.caosdb.entity.v1.FileTransmissionSettings} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.FileTransmissionSettings, 4));
};


/**
 * @param {?proto.caosdb.entity.v1.FileTransmissionSettings|undefined} value
 * @return {!proto.caosdb.entity.v1.RegisterFileUploadResponse} returns this
*/
proto.caosdb.entity.v1.RegisterFileUploadResponse.prototype.setUploadSettings = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.RegisterFileUploadResponse} returns this
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.prototype.clearUploadSettings = function() {
  return this.setUploadSettings(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.RegisterFileUploadResponse.prototype.hasUploadSettings = function() {
  return jspb.Message.getField(this, 4) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.FileUploadRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.FileUploadRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.FileUploadRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileUploadRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    chunk: (f = msg.getChunk()) && proto.caosdb.entity.v1.FileChunk.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.FileUploadRequest}
 */
proto.caosdb.entity.v1.FileUploadRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.FileUploadRequest;
  return proto.caosdb.entity.v1.FileUploadRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.FileUploadRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.FileUploadRequest}
 */
proto.caosdb.entity.v1.FileUploadRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.FileChunk;
      reader.readMessage(value,proto.caosdb.entity.v1.FileChunk.deserializeBinaryFromReader);
      msg.setChunk(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.FileUploadRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.FileUploadRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.FileUploadRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileUploadRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getChunk();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.FileChunk.serializeBinaryToWriter
    );
  }
};


/**
 * optional FileChunk chunk = 1;
 * @return {?proto.caosdb.entity.v1.FileChunk}
 */
proto.caosdb.entity.v1.FileUploadRequest.prototype.getChunk = function() {
  return /** @type{?proto.caosdb.entity.v1.FileChunk} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.FileChunk, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.FileChunk|undefined} value
 * @return {!proto.caosdb.entity.v1.FileUploadRequest} returns this
*/
proto.caosdb.entity.v1.FileUploadRequest.prototype.setChunk = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.FileUploadRequest} returns this
 */
proto.caosdb.entity.v1.FileUploadRequest.prototype.clearChunk = function() {
  return this.setChunk(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.FileUploadRequest.prototype.hasChunk = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.FileUploadResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.FileUploadResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.FileUploadResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileUploadResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.FileUploadResponse}
 */
proto.caosdb.entity.v1.FileUploadResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.FileUploadResponse;
  return proto.caosdb.entity.v1.FileUploadResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.FileUploadResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.FileUploadResponse}
 */
proto.caosdb.entity.v1.FileUploadResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.caosdb.entity.v1.TransmissionStatus} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.FileUploadResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.FileUploadResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.FileUploadResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileUploadResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * optional TransmissionStatus status = 1;
 * @return {!proto.caosdb.entity.v1.TransmissionStatus}
 */
proto.caosdb.entity.v1.FileUploadResponse.prototype.getStatus = function() {
  return /** @type {!proto.caosdb.entity.v1.TransmissionStatus} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.caosdb.entity.v1.TransmissionStatus} value
 * @return {!proto.caosdb.entity.v1.FileUploadResponse} returns this
 */
proto.caosdb.entity.v1.FileUploadResponse.prototype.setStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.FileDownloadRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.FileDownloadRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.FileDownloadRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileDownloadRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    fileTransmissionId: (f = msg.getFileTransmissionId()) && proto.caosdb.entity.v1.FileTransmissionId.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.FileDownloadRequest}
 */
proto.caosdb.entity.v1.FileDownloadRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.FileDownloadRequest;
  return proto.caosdb.entity.v1.FileDownloadRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.FileDownloadRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.FileDownloadRequest}
 */
proto.caosdb.entity.v1.FileDownloadRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.caosdb.entity.v1.FileTransmissionId;
      reader.readMessage(value,proto.caosdb.entity.v1.FileTransmissionId.deserializeBinaryFromReader);
      msg.setFileTransmissionId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.FileDownloadRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.FileDownloadRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.FileDownloadRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileDownloadRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFileTransmissionId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.caosdb.entity.v1.FileTransmissionId.serializeBinaryToWriter
    );
  }
};


/**
 * optional FileTransmissionId file_transmission_id = 1;
 * @return {?proto.caosdb.entity.v1.FileTransmissionId}
 */
proto.caosdb.entity.v1.FileDownloadRequest.prototype.getFileTransmissionId = function() {
  return /** @type{?proto.caosdb.entity.v1.FileTransmissionId} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.FileTransmissionId, 1));
};


/**
 * @param {?proto.caosdb.entity.v1.FileTransmissionId|undefined} value
 * @return {!proto.caosdb.entity.v1.FileDownloadRequest} returns this
*/
proto.caosdb.entity.v1.FileDownloadRequest.prototype.setFileTransmissionId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.FileDownloadRequest} returns this
 */
proto.caosdb.entity.v1.FileDownloadRequest.prototype.clearFileTransmissionId = function() {
  return this.setFileTransmissionId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.FileDownloadRequest.prototype.hasFileTransmissionId = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.caosdb.entity.v1.FileDownloadResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.caosdb.entity.v1.FileDownloadResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.caosdb.entity.v1.FileDownloadResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileDownloadResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: jspb.Message.getFieldWithDefault(msg, 1, 0),
    chunk: (f = msg.getChunk()) && proto.caosdb.entity.v1.FileChunk.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.caosdb.entity.v1.FileDownloadResponse}
 */
proto.caosdb.entity.v1.FileDownloadResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.caosdb.entity.v1.FileDownloadResponse;
  return proto.caosdb.entity.v1.FileDownloadResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.caosdb.entity.v1.FileDownloadResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.caosdb.entity.v1.FileDownloadResponse}
 */
proto.caosdb.entity.v1.FileDownloadResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.caosdb.entity.v1.TransmissionStatus} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    case 2:
      var value = new proto.caosdb.entity.v1.FileChunk;
      reader.readMessage(value,proto.caosdb.entity.v1.FileChunk.deserializeBinaryFromReader);
      msg.setChunk(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.caosdb.entity.v1.FileDownloadResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.caosdb.entity.v1.FileDownloadResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.caosdb.entity.v1.FileDownloadResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.caosdb.entity.v1.FileDownloadResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getChunk();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.caosdb.entity.v1.FileChunk.serializeBinaryToWriter
    );
  }
};


/**
 * optional TransmissionStatus status = 1;
 * @return {!proto.caosdb.entity.v1.TransmissionStatus}
 */
proto.caosdb.entity.v1.FileDownloadResponse.prototype.getStatus = function() {
  return /** @type {!proto.caosdb.entity.v1.TransmissionStatus} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.caosdb.entity.v1.TransmissionStatus} value
 * @return {!proto.caosdb.entity.v1.FileDownloadResponse} returns this
 */
proto.caosdb.entity.v1.FileDownloadResponse.prototype.setStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional FileChunk chunk = 2;
 * @return {?proto.caosdb.entity.v1.FileChunk}
 */
proto.caosdb.entity.v1.FileDownloadResponse.prototype.getChunk = function() {
  return /** @type{?proto.caosdb.entity.v1.FileChunk} */ (
    jspb.Message.getWrapperField(this, proto.caosdb.entity.v1.FileChunk, 2));
};


/**
 * @param {?proto.caosdb.entity.v1.FileChunk|undefined} value
 * @return {!proto.caosdb.entity.v1.FileDownloadResponse} returns this
*/
proto.caosdb.entity.v1.FileDownloadResponse.prototype.setChunk = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.caosdb.entity.v1.FileDownloadResponse} returns this
 */
proto.caosdb.entity.v1.FileDownloadResponse.prototype.clearChunk = function() {
  return this.setChunk(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.caosdb.entity.v1.FileDownloadResponse.prototype.hasChunk = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * @enum {number}
 */
proto.caosdb.entity.v1.AtomicDataType = {
  ATOMIC_DATA_TYPE_UNSPECIFIED: 0,
  ATOMIC_DATA_TYPE_TEXT: 1,
  ATOMIC_DATA_TYPE_DOUBLE: 2,
  ATOMIC_DATA_TYPE_DATETIME: 3,
  ATOMIC_DATA_TYPE_INTEGER: 4,
  ATOMIC_DATA_TYPE_BOOLEAN: 5
};

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.SpecialValue = {
  SPECIAL_VALUE_UNSPECIFIED: 0,
  SPECIAL_VALUE_EMPTY_STRING: 1
};

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.EntityRole = {
  ENTITY_ROLE_UNSPECIFIED: 0,
  ENTITY_ROLE_RECORD_TYPE: 1,
  ENTITY_ROLE_RECORD: 2,
  ENTITY_ROLE_PROPERTY: 3,
  ENTITY_ROLE_FILE: 4
};

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.MessageCode = {
  MESSAGE_CODE_UNSPECIFIED: 0,
  MESSAGE_CODE_UNKNOWN: 1,
  MESSAGE_CODE_ENTITY_DOES_NOT_EXIST: 2,
  MESSAGE_CODE_ENTITY_HAS_NO_PROPERTIES: 3,
  MESSAGE_CODE_ENTITY_HAS_BEEN_DELETED_SUCCESSFULLY: 4,
  MESSAGE_CODE_ENTITY_HAS_UNQUALIFIED_PROPERTIES: 5,
  MESSAGE_CODE_ENTITY_HAS_UNQUALIFIED_PARENTS: 6,
  MESSAGE_CODE_ENTITY_HAS_NO_ID: 7,
  MESSAGE_CODE_REQUIRED_BY_PERSISTENT_ENTITY: 8,
  MESSAGE_CODE_PROPERTY_HAS_NO_DATA_TYPE: 9,
  MESSAGE_CODE_ENTITY_HAS_NO_DESCRIPTION: 10,
  MESSAGE_CODE_ENTITY_HAS_NO_NAME: 11,
  MESSAGE_CODE_OBLIGATORY_PROPERTY_MISSING: 12,
  MESSAGE_CODE_ENTITY_HAS_NO_PARENTS: 13,
  MESSAGE_CODE_FILE_HAS_NO_TARGET_PATH: 14,
  MESSAGE_CODE_TARGET_PATH_NOT_ALLOWED: 15,
  MESSAGE_CODE_TARGET_PATH_EXISTS: 16,
  MESSAGE_CODE_PROPERTY_HAS_NO_UNIT: 17,
  MESSAGE_CODE_CANNOT_PARSE_VALUE: 18,
  MESSAGE_CODE_CHECKSUM_TEST_FAILED: 19,
  MESSAGE_CODE_SIZE_TEST_FAILED: 20,
  MESSAGE_CODE_CANNOT_CREATE_PARENT_FOLDER: 21,
  MESSAGE_CODE_FILE_HAS_NOT_BEEN_UPLOAED: 22,
  MESSAGE_CODE_CANNOT_MOVE_FILE_TO_TARGET_PATH: 23,
  MESSAGE_CODE_CANNOT_PARSE_DATETIME_VALUE: 24,
  MESSAGE_CODE_CANNOT_PARSE_DOUBLE_VALUE: 25,
  MESSAGE_CODE_CANNOT_PARSE_INT_VALUE: 26,
  MESSAGE_CODE_CANNOT_PARSE_BOOL_VALUE: 27,
  MESSAGE_CODE_FILE_NOT_FOUND: 28,
  MESSAGE_CODE_WARNING_OCCURED: 29,
  MESSAGE_CODE_ENTITY_NAME_IS_NOT_UNIQUE: 30,
  MESSAGE_CODE_QUERY_EXCEPTION: 31,
  MESSAGE_CODE_TRANSACTION_ROLL_BACK: 32,
  MESSAGE_CODE_UNKNOWN_UNIT: 34,
  MESSAGE_CODE_AUTHORIZATION_ERROR: 35,
  MESSAGE_CODE_REFERENCE_IS_NOT_ALLOWED_BY_DATA_TYPE: 36,
  MESSAGE_CODE_ENTITY_NAME_DUPLICATES: 37,
  MESSAGE_CODE_DATA_TYPE_NAME_DUPLICATES: 38,
  MESSAGE_CODE_ENTITY_HAS_NO_NAME_OR_ID: 39,
  MESSAGE_CODE_AFFILIATION_ERROR: 40,
  MESSAGE_CODE_QUERY_PARSING_ERROR: 41,
  MESSAGE_CODE_NAME_PROPERTIES_MUST_BE_TEXT: 42,
  MESSAGE_CODE_PARENT_DUPLICATES_WARNING: 43,
  MESSAGE_CODE_PARENT_DUPLICATES_ERROR: 44,
  MESSAGE_CODE_ATOMICITY_ERROR: 45,
  MESSAGE_CODE_NO_SUCH_ENTITY_ROLE: 46,
  MESSAGE_CODE_REQUIRED_BY_UNQUALIFIED: 47,
  MESSAGE_CODE_ENTITY_HAS_UNQUALIFIED_REFERENCE: 48,
  MESSAGE_CODE_REFERENCED_ENTITY_DOES_NOT_EXIST: 49,
  MESSAGE_CODE_REFERENCE_NAME_DUPLICATES: 50,
  MESSAGE_CODE_DATA_TYPE_INHERITANCE_AMBIGUOUS: 51,
  MESSAGE_CODE_DATA_TYPE_DOES_NOT_ACCEPT_COLLECTION_VALUES: 52,
  MESSAGE_CODE_CANNOT_PARSE_UNIT: 53,
  MESSAGE_CODE_ADDITIONAL_PROPERTY: 54,
  MESSAGE_CODE_PROPERTY_WITH_DATA_TYPE_OVERRIDE: 55,
  MESSAGE_CODE_PROPERTY_WITH_DESCRIPTION_OVERRIDE: 56,
  MESSAGE_CODE_PROPERTY_WITH_NAME_OVERRIDE: 57,
  MESSAGE_CODE_INTEGER_VALUE_OUT_OF_RANGE: 58,
  MESSAGE_CODE_INTEGRITY_VIOLATION: 59
};

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.Importance = {
  IMPORTANCE_UNSPECIFIED: 0,
  IMPORTANCE_OBLIGATORY: 1,
  IMPORTANCE_RECOMMENDED: 2,
  IMPORTANCE_SUGGESTED: 3,
  IMPORTANCE_FIX: 4
};

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.RegistrationStatus = {
  REGISTRATION_STATUS_UNSPECIFIED: 0,
  REGISTRATION_STATUS_ACCEPTED: 1,
  REGISTRATION_STATUS_REJECTED: 2
};

/**
 * @enum {number}
 */
proto.caosdb.entity.v1.TransmissionStatus = {
  TRANSMISSION_STATUS_UNSPECIFIED: 0,
  TRANSMISSION_STATUS_SUCCESS: 1,
  TRANSMISSION_STATUS_ERROR: 2,
  TRANSMISSION_STATUS_GO_ON: 3
};

goog.object.extend(exports, proto.caosdb.entity.v1);
