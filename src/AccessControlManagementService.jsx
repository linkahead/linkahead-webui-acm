import { api } from './AccessControlManagementApi';

const _client = new api.v1alpha1.AccessControlManagementServiceClient("/api", null, null);

function _convertPermissionRuleToMessage(rule) {
  const result = new api.v1alpha1.PermissionRule();
  result.setPermission(rule.permission);
  result.setGrant(rule.grant);
  result.setPriority(rule.priority);
  return result;
}

function _convertRoleToMessage(role) {
  const role_message = new api.v1alpha1.Role();
  role_message.setName(role.name);
  role_message.setDescription(role.description);
  role_message.setPermissionRulesList(role.permission_rules.map(_convertPermissionRuleToMessage))
  return role_message;
}

function _convertMessageToRole(role_message, permissions, capabilities) {
  const role = new _Role(role_message.getName(), role_message.getDescription())

  if(role_message.getPermissionRulesList()) {
    role.permission_rules = role_message.getPermissionRulesList().map((rule) => rule.toObject());
  }

  if (permissions) {
    const p = api.v1alpha1.RolePermissions;
    for (let permission of permissions) {
      if ( permission === p.ROLE_PERMISSIONS_DELETE) {
        role.permissions.delete = true;
      } else if ( permission === p.ROLE_PERMISSIONS_UPDATE_DESCRIPTION) {
        role.permissions.update_description = true;
      } else if ( permission === p.ROLE_PERMISSIONS_UPDATE_PERMISSION_RULES) {
        role.permissions.update_permission_rules = true;
      } else if ( permission === p.ROLE_PERMISSIONS_ASSIGN) {
        role.permissions.assign= true;
      }
    }
  }

  if (capabilities) {
    const c = api.v1alpha1.RoleCapabilities;
    for (let capability of capabilities) {
      if ( capability === c.ROLE_CAPABILITIES_DELETE) {
        role.capabilities.delete = true;
      } else if ( capability === c.ROLE_CAPABILITIES_UPDATE_PERMISSION_RULES) {
        role.capabilities.update_permission_rules = true;
      } else if ( capability === c.ROLE_CAPABILITIES_ASSIGN) {
        role.capabilities.assign = true;
      }
    }
  }

  return role;
}

function _DeleteRole(name) {
  const request = new api.v1alpha1.DeleteSingleRoleRequest();
  request.setName(name);

  return (res_cb, err_cb) => {
    _client.deleteSingleRole(request, {}, (error_response, response) => {
      if(error_response) {
        err_cb(error_response);
      }
      if(response) {
        res_cb(response.toObject());
      } else {
        res_cb();
      }
    })
  }
}

function _convertUserToMessage(user) {
  const user_message = new api.v1alpha1.User();

  user_message.setName(user.name);
  user_message.setRealm(user.realm);
  if(user.email != null && typeof user.email !== "undefined") {
    const email_setting = new api.v1alpha1.EmailSetting();
    email_setting.setEmail(user.email);
    user_message.setEmailSetting(email_setting);
  }
  if(user.active) {
    user_message.setStatus(api.v1alpha1.UserStatus.USER_STATUS_ACTIVE);
  } else {
    user_message.setStatus(api.v1alpha1.UserStatus.USER_STATUS_INACTIVE);
  }
  // TODO user_message.setEntitySetting(...)
  if(user.roles) {
    user_message.setRolesList(user.roles);
  }
  return user_message;
}

function _convertMessageToUser(user_message, permissions, capabilities) {
  const email = user_message.hasEmailSetting() ? user_message.getEmailSetting().getEmail() : undefined;
  // TODO ... user_message.getEntitySetting()
  const user = new _User(user_message.getRealm(), user_message.getName(),
    user_message.getStatus() === api.v1alpha1.UserStatus.USER_STATUS_ACTIVE,
    email, user_message.getRolesList());

  if (permissions) {
    const p = api.v1alpha1.UserPermissions;
    for (let permission of permissions) {
      if ( permission === p.USER_PERMISSIONS_DELETE) {
        user.permissions.delete = true;
      } else if ( permission === p.USER_PERMISSIONS_UPDATE_PASSWORD) {
        user.permissions.update_password = true;
      } else if ( permission === p.USER_PERMISSIONS_UPDATE_EMAIL) {
        user.permissions.update_email = true;
      } else if ( permission === p.USER_PERMISSIONS_UPDATE_STATUS) {
        user.permissions.update_status = true;
      } else if ( permission === p.USER_PERMISSIONS_UPDATE_ROLES) {
        user.permissions.update_roles = true;
      } else if ( permission === p.USER_PERMISSIONS_UPDATE_ENTITY) {
        user.permissions.update_entity = true;
      }
    }
  }

  if (capabilities) {
    const c = api.v1alpha1.UserCapabilities;
    for (let capability of capabilities) {
      if (capability === c.USER_CAPABILITIES_DELETE) {
        user.capabilities.delete = true;
      } else if (capability === c.USER_CAPABILITIES_UPDATE_PASSWORD) {
        user.capabilities.update_password = true;
      }
    }
  }
  return user;
}

function _CreateUser(user, password) {
  const request = new api.v1alpha1.CreateSingleUserRequest();
  request.setUser(_convertUserToMessage(user));
  if(password != null && typeof password !== "undefined") {
    const password_setting = new api.v1alpha1.PasswordSetting();
    password_setting.setPassword(password);
    request.setPasswordSetting(password_setting);
  }

  return (res_cb, err_cb) => {
    _client.createSingleUser(request, {}, (error_response, response) => {
      if(error_response) {
        err_cb(error_response);
      }
      if(response) {
        res_cb(response.toObject());
      } else {
        res_cb();
      }
    })
  }
}


class _User {
  constructor(realm, name, active, email, roles) {
    this.active = active;
    this.realm = realm;
    this.name = name;
    this.email = email;
    this.roles = roles;
    this.permissions = {};
    this.capabilities = {};
  }

  /**
   * Return true iff the user of the current session has the permission to update at least one property of this user.
   */
  updatable() {
    return this.permissions.update_entity || this.permissions.update_email ||
      this.permissions.update_status || (this.capabilities.update_password &&
        this.permissions.update_password) || this.permissions.update_roles;
  }

  /**
   * Return true iff this user can be deleted and the user of the current
   * session has the permission to do so.
   */
  deletable() {
    return this.permissions.delete && this.capabilities.delete;
  }
}

class _Role {
  constructor(name, description, permission_rules) {
    this.name = name;
    this.description = description;
    this.permission_rules = permission_rules;
    this.users = undefined;
    this.permissions = {};
    this.capabilities = {};
  }

  updatable() {
    return this.permissions.update_description ||
      (this.capabilities.update_permission_rules &&
        this.permissions.update_permission_rules)
  }

  deletable() {
    return this.permissions.delete && this.capabilities.delete;
  }

  assignable() {
    return this.permissions.assign && this.capabilities.assign;
  }
}

export class AccessControlManagementService {

  constructor(url) {
    this.url = url || "/api";
  }

  async _UpdateRole(role) {
    const client = new api.v1alpha1.AccessControlManagementServicePromiseClient(this.url, null, null);
    const request = new api.v1alpha1.UpdateSingleRoleRequest();
    request.setRole(_convertRoleToMessage(role));

    const response = await client.updateSingleRole(request, {});
    return response.toObject();
  }


  async _CreateRole(role) {
    const client = new api.v1alpha1.AccessControlManagementServicePromiseClient(this.url, null, null);
    const request = new api.v1alpha1.CreateSingleRoleRequest();
    request.setRole(_convertRoleToMessage(role));

    const response = await client.createSingleRole(request, {});
    return response.toObject();
  }

  async _ListRoles() {
    const client = new api.v1alpha1.AccessControlManagementServicePromiseClient(this.url, null, null);
    const request = new api.v1alpha1.ListRolesRequest();

    const response = await client.listRoles(request, {});
    const roles_list = response.getRolesList().map(item => _convertMessageToRole(item.getRole(), item.getPermissionsList(), item.getCapabilitiesList()));
    return roles_list;
  }

  async _getRole(name) {
    const client = new api.v1alpha1.AccessControlManagementServicePromiseClient(this.url, null, null);

    const request = new api.v1alpha1.RetrieveSingleRoleRequest();
    request.setName(name);

    const response = await client.retrieveSingleRole(request, {});

    const role = _convertMessageToRole(response.getRole(), response.getPermissionsList(), response.getCapabilitiesList());

    if(response.getUsersList()) {
      role.users = response.getUsersList().map((user) => user.toObject());
    }

    return role;
  }

  async _ListKnownPermissions() {
    const client = new api.v1alpha1.AccessControlManagementServicePromiseClient(this.url, null, null);
    const request = new api.v1alpha1.ListKnownPermissionsRequest();

    const response = await client.listKnownPermissions(request, {});
    return response.toObject().permissionsList;
  }

  async _UpdateUser(user, password) {
    const client = new api.v1alpha1.AccessControlManagementServicePromiseClient(this.url, null, null);
    const request = new api.v1alpha1.UpdateSingleUserRequest();
    request.setUser(_convertUserToMessage(user));
    if(password != null && typeof password !== "undefined") {
      const password_setting = new api.v1alpha1.PasswordSetting();
      password_setting.setPassword(password);
      request.setPasswordSetting(password_setting);
    }

    return client.updateSingleUser(request, {});
  }

  async _DeleteUser(realm, name) {
    const client = new api.v1alpha1.AccessControlManagementServicePromiseClient(this.url, null, null);
    const request = new api.v1alpha1.DeleteSingleUserRequest();
    request.setRealm(realm);
    request.setName(name);

    return client.deleteSingleUser(request, {});
  }

  async _getUser(realm, name) {
    const client = new api.v1alpha1.AccessControlManagementServicePromiseClient(this.url, null, null);
    const request = new api.v1alpha1.RetrieveSingleUserRequest();
    request.setRealm(realm);
    request.setName(name);

    const response = await client.retrieveSingleUser(request, {})
    return _convertMessageToUser(response.getUser(), response.getPermissionsList(), response.getCapabilitiesList());
  }

  async _ListUsers() {
    const client = new api.v1alpha1.AccessControlManagementServicePromiseClient(this.url, null, null);
    const request = new api.v1alpha1.ListUsersRequest();

    const response = await client.listUsers(request, {});
    return response.getUsersList().map((user) => _convertMessageToUser(user));
  }

  async getUserList() {
    return await this._ListUsers();
  }

  async getUser(realm, name) {
    return await this._getUser(realm, name);
  }

  async getRole(name) {
    return await this._getRole(name);
  }

  async listRoles() {
    return await this._ListRoles();
  }

  /**
   * Create a new user in the internal realm.
   */
  createUser({name, active, email, password, roles}) {
    const user = new _User("CaosDB", name, active, email, roles);
    return new Promise(_CreateUser(user, password));
  }

  /**
   * Update a user.
   */
  async updateUser({realm, name, email, active, password, roles}) {
    const user = new _User(realm, name, active, email, roles);

    return await this._UpdateUser(user, password);
  }

  async deleteUser(realm, name) {
    return await this._DeleteUser(realm, name);
  }

  /**
   * Create a new role.
   */
  async createRole({name, description, permission_rules}) {
    const role = new _Role(name, description, permission_rules);
    return await this._CreateRole(role);
  }

  deleteRole(name) {
    return new Promise(_DeleteRole(name));
  }

  async updateRole({name, description, permission_rules}) {
    const role = new _Role(name, description, permission_rules);
    return await this._UpdateRole(role);
  }

  async listKnownPermissions() {
    return await this._ListKnownPermissions();
  }
}
