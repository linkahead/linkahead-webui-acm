import React, { useState } from 'react';
import { Await } from "./Await";
import { useHistory } from "react-router-dom";
import { AccessControlManagementService } from "../AccessControlManagementService";
import { RolesList } from "./UserList";

const toRolesSelect = (preselected, readOnly) => {
  return (roles_list) => {
    const roles_options = roles_list
      .filter((role) => (preselected.indexOf(role.name) !== -1 || role.assignable()))
      .map((role) => <option key={role.name} >{role.name}</option>);

    // TODO handle permissions for role assignment/unassignment
    return (
      <select name="roles" defaultValue={preselected} readOnly={readOnly} multiple>
        {roles_options}
      </select>);
  }
}

export function UserForm({submitLabel, acmService, rolesList, values, readOnly, onCancel}) {
  values = values || {active: true, roles: []};
  readOnly = readOnly || {};
  const [error, setError] = useState(null);
  const history = useHistory();
  const cancelLabel = "Cancel";
  onCancel = onCancel || (() => {
    history.goBack();
  });
  const handleSubmit = async (event) => {
    event.preventDefault();
    const name = event.target.name.value;
    const active = event.target.active.checked;
    const email = event.target.email.value || null;
    const password = event.target.password ? (event.target.password.value || null) : null;
    const roles = [...event.target.roles.options].filter(option => option.selected).map(option => option.value);

    try {
      // TODO show waiting notification
      await acmService({name: name, active: active, email: email, password: password, roles: roles, realm: values.realm});
      history.goBack();
    } catch(error) {
      setError(error.message);
    }

  }
  const active_switch = readOnly.active ?
    <input name="active" type="hidden" defaultValue={values.active}/> :
    (<div>
      <label>Active
        <input name="active" type="checkbox" defaultChecked={values.active} />
      </label>
    </div>);

  const name_input = readOnly.name ?
    (<div><span>Name</span> {values.name}
      <input name="name" type="hidden" defaultValue={values.name}/>
    </div>) :
    (<div>
      <label>Name
        <input name="name" defaultValue={values.name}/>
      </label>
    </div>)

  const email_input = readOnly.email ?
    (<div><span>Email</span> {values.email}
      <input name="email" type="hidden" defaultValue={values.email}/>
    </div>) :
    (<div>
      <label>Email
        <input name="email" defaultValue={values.email}/>
      </label>
    </div>);

  const password_input = readOnly.password ?
    (<div><span>Password</span> ******** </div>) :
    (<div>
      <label>Password
        <input name="password" type="password"/>
      </label>
    </div>);

  const roles_select = readOnly.roles ?
    (<div><span>Roles</span> <RolesList roles={values.roles}/></div>) :
    (<div>
      <label>Roles
        <Await promise={rolesList} then={toRolesSelect(values.roles, !!readOnly.roles)} />
      </label>
    </div>);


  return (
    <form onSubmit={handleSubmit}>
      {active_switch}
      {name_input}
      {email_input}
      {password_input}
      {roles_select}
      <div>
        <button type="submit">{submitLabel}</button>
        <button type="button" onClick={onCancel}>{cancelLabel}</button>
      </div>
      {error}
    </form>
  );
}

export function CreateUser() {
  const acm = new AccessControlManagementService();
  return (
    <div className="caosdb-create-user">
      <h1>Create a new user</h1>
      <UserForm submitLabel={"Create"} rolesList={acm.listRoles()} acmService={acm.createUser} />
    </div>
  );
}
