import React from 'react';
import { Link } from "react-router-dom";
import { Await } from "./Await";
import { LoadingInfo } from "@indiscale/caosdb-webui-core-components";
import { useParams } from "react-router-dom";
import { AccessControlManagementService } from "../AccessControlManagementService";

export function RolesList({roles}) {
  return (<ul> {roles.map(
      (role) => <li key={role}>
        <Link to={`/roles/${role}`}>{role}</Link>
      </li>)}
    </ul>)
}

export function SingleUser() {
  let params = useParams();
  const acm = new AccessControlManagementService();
  const userView = (user) => {
    const update_link = user.updatable() ? <Link to={
      {
        pathname: `/users/edit/${user.realm}/${user.name}`,
        state: {
          values: {
            realm: user.realm,
            name: user.name,
            active: user.active,
            roles: user.roles,
            email: user.email
          },
          readOnly: {
            name: true,
            active: !user.permissions.update_state,
            roles: !user.permissions.update_roles,
            email: !user.permissions.update_email,
            password: !user.permissions.update_password || !user.capabilities.update_password
          }
        }
      }}>Update</Link> : undefined;
    const delete_link = user.deletable() ? <Link to={
      `/users/delete/${user.realm}/${user.name}`}>Delete</Link> : undefined;

    return (
      <div className="caosdb-single-user">
        <h1>{user.name}@{user.realm}</h1>
        <p>Email: {user.email}</p>
        {user.roles ?
          <div>Roles:
            <RolesList roles={user.roles}/>
          </div>
        : {}}
        {update_link}
        {delete_link}
      </div>
    );
  }
  return (
    <Await promise={acm.getUser(params.realm, params.id)} then={userView} />
  );
}

export function UserListItem({user}) {
  return (
    <tr>
      <td>{"" + user.active}</td>
      <td>{user.realm}</td>
      <td>
        <Link to={`${user.realm}/${user.name}`}>{user.name}</Link>
      </td>
      <td><Link to={`mailto:${user.email}`}>{user.email}</Link></td>
    </tr>
  );
}

export class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: <LoadingInfo/>,
      error: null,
    };

  }
  render() {
    const acm = new AccessControlManagementService();
    const toUserListItems = (response) => {
        this.setState({...this.state, loading: undefined});
        return response.map((user) => <UserListItem key={`${user.realm}@${user.name}`} user={user}/>
    )}

    const onError = ({message, stack, code, metadata}) => {
      // implicitely remove this.state.loading
      this.setState({error: message});
    }
    return (
      <div className="caosdb-user-list-component">
        <h1>User List</h1>
        {this.state.error ? this.state.error :
          <React.Fragment>
            <table>
              <thead>
                <tr>
                  <th>Active</th><th>Realm</th><th>Name</th><th>Email</th>
                </tr>
              </thead>
              <tbody>
                <Await promise={acm.getUserList()} then={toUserListItems} catch={onError} loading={false} />
              </tbody>
            </table>
          </React.Fragment>
        }
      </div>
    );
  }
}
