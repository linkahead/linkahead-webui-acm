import React from "react";
import { AccessControlManagementService } from "../AccessControlManagementService";
import { RoleForm } from "./CreateRole";
import { useHistory } from "react-router-dom";
import { useLocation } from "react-router";

export function EditRole() {
  var location = useLocation();

  var values = location.state.values;
  var readOnly = location.state.readOnly;

  const acm = new AccessControlManagementService();
  const history = useHistory();
  const onCancel = () => history.goBack();
  return (<div>
    <h1>Edit Role</h1>
    <RoleForm submitLabel={"Save"}
              acmService={acm.updateRole}
              onCancel={onCancel}
              readOnly={readOnly}
              defaultValues={values}
              />
    </div>);
}
