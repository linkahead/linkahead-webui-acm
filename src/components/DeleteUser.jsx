import React, { useState } from 'react';
import { useHistory, useParams } from "react-router-dom";
import { AccessControlManagementService } from "../AccessControlManagementService";


export function DeleteUser() {
  let params = useParams();
  const [error, setError] = useState(null);
  const history = useHistory();

  const acm = new AccessControlManagementService();
  const delete_callback = async () => {
    try {
      await acm.deleteUser(params.realm, params.name);
      history.push("/users");
    } catch (error) {
      setError(error.message);
    }
  }

  return (<div><h1>Delete user {params.name}@{params.realm}</h1>
    <p>Do you really want to delete this user?</p>
    <button onClick={delete_callback}>Delete {params.name}@{params.realm}</button>
    {error ? <div>{error}</div> : undefined}
    </div>);
}
