import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import { useForm, useFieldArray } from "react-hook-form";
import { AccessControlManagementService } from "../AccessControlManagementService";
import { PermissionRulesView } from "./SingleRoleView";


export function PermissionsFormPart({defaultValues}) {
  const initForm = defaultValues && defaultValues.length > 0 ? {
    defaultValues: {
      permissionRulesFormPart: defaultValues
    }
  } : {};
  const { control, register } = useForm(initForm);
  const { fields, append, remove } = useFieldArray({
          control,
          name: "permissionRulesFormPart",
  });

  const remove_button = (index) => <button type="button" onClick={() => remove(index)}>-</button>

  const append_button = <button type="button" onClick={()=>append({permission: undefined})}>+</button>

    const grant_switch = (index) => <input {...register(`permissionRulesFormPart.${index}.grant`)}  className="caosdb-permission-grant-field" type="checkbox" />

    const priority_switch = (index) => <input {...register(`permissionRulesFormPart.${index}.priority`)}  className="caosdb-permission-priority-field" type="checkbox" />

  return (
    <div className="caosdb-permissions-form-part"><h4>Permissions</h4>
      <ul> {
        fields.map((field, index) => (
          <li key={field.id} className="caosdb-permission-rule-field">
            <input {...register(`permissionRulesFormPart.${index}.permission`)} className="caosdb-permission-name-field" />
            <label>{priority_switch(index)} Priority</label>
            <label>{grant_switch(index)} Grant</label>
            { remove_button(index) }
          </li>
        ))
      } </ul>
      { append_button }
    </div>
  );

}

function getPermissionRuleFromField(field) {
  return {
    permission: field.getElementsByClassName("caosdb-permission-name-field")[0].value,
    priority: field.getElementsByClassName("caosdb-permission-priority-field")[0].checked,
    grant: field.getElementsByClassName("caosdb-permission-grant-field")[0].checked,
  }
}

function getPermissionRulesFromForm(formPart) {
  const result = [];
  for (let field of formPart.getElementsByClassName("caosdb-permission-rule-field")) {
    result.push(
      getPermissionRuleFromField(field)
    );
  }
  return result;
}

export function RoleForm({submitLabel, acmService, defaultValues, onCancel, readOnly}) {
  defaultValues = defaultValues || {};
  readOnly = readOnly || {};
  const history = useHistory();
  const [error, setError] = useState(null);
  const handleSubmit = async (event) => {
    event.preventDefault();
    const name = event.target.name.value;
    const description = event.target.description.value;
    const permission_rules = getPermissionRulesFromForm(event.target.getElementsByClassName("caosdb-permissions-form-part")[0]);

    try {
      // TODO show waiting notification
      await acmService({name: name, description: description, permission_rules: permission_rules});
      history.goBack();
    } catch(error) {
      setError(error.message);
    }
  }

  const cancelButton = onCancel ? <button type="button" onClick={onCancel}>Cancel</button> : undefined;
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label>Name
          <input name="name" defaultValue={defaultValues.name} readOnly={readOnly.name}/>
        </label>
      </div>
      <div>
        <label>Description
          <input name="description" defaultValue={defaultValues.description} readOnly={readOnly.description}/>
        </label>
      </div>
      { readOnly.permission_rules ?
        <PermissionRulesView permission_rules={defaultValues.permission_rules} /> :
        <PermissionsFormPart defaultValues={defaultValues.permission_rules}/> }
      {error}
      <div>
        {cancelButton}
        <button type="submit">{submitLabel}</button>
      </div>
    </form>
  );
}

export function CreateRole() {
  const acm = new AccessControlManagementService();
  const history = useHistory();
  const onCancel = () => history.goBack();
  return (
    <div className="caosdb-create-role">
      <h1>Create a new role</h1>
      <RoleForm submitLabel={"Create"}
                onCancel={onCancel}
                acmService={acm.createRole} />
    </div>
  );
}
