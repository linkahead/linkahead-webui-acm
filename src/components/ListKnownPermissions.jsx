import React from 'react';
import { Await } from "./Await";
import { AccessControlManagementService } from "../AccessControlManagementService";


function createTable(permissions) {
  const createRow = (item) => {
    return (<tr><td>{item.permission}</td><td>{item.description}</td></tr>)
  }
  return (<table><thead><tr><th>Permission</th><th>Description</th></tr></thead><tbody>{permissions.map(createRow)}</tbody></table>);
}

function onError(error) {
  return (<div>{error.message}</div>);
}


export function ListKnownPermissions(props) {
  const acm = new AccessControlManagementService();
  return (<div><h1>List Known Permissions</h1><Await promise={acm.listKnownPermissions()} then={createTable} catch={onError} /></div>);
}
