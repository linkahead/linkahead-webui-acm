import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import entity_api_v1 from '../generated/proto/caosdb/entity/v1/main_grpc_web_pb.js';

async function retrieveEntityAcl(id) {
  const client = new entity_api_v1.EntityTransactionServicePromiseClient("/api", null, null);
  const request = new entity_api_v1.MultiRetrieveEntityACLRequest();
  request.addId(id);
  const response = await client.multiRetrieveEntityACL(request, {});

  const result = response.toObject().aclsList[0];
  console.log(result);
  return result
}

function createEntityAclBody(acl) {
  console.log(acl);
  const grant = (aci) => aci.grant ? "Grant " : "Deny ";
  const priority = (aci) => aci.priority ? "[P] " : undefined;
  const role = (aci) => aci.role + " ";
  const permission = (aci) => aci.permission.name;
  return (<div>{acl.rulesList.map((aci) => <div>{priority(aci)} {grant(aci)} {role(aci)} {permission(aci)}</div>)}</div>)
}

export function SingleEntityAcl() {
  const [acl, setAcl] = useState(null);
  const params = useParams();
  var acl_show = "...";

  if (acl == null) {
    retrieveEntityAcl(params.id)
      .then(setAcl)
      .catch((e) => setAcl(e.message));
  } else {
    acl_show = createEntityAclBody(acl);
  }

  return (<div><h1>Entity ACL</h1>{acl_show}</div>);
}
