import React from 'react';
import { Await } from "./Await";
import { Link, useParams } from "react-router-dom";
import { AccessControlManagementService } from "../AccessControlManagementService";


export function SinglePermissionRuleView({permission_rule}) {
  const grant = permission_rule.grant ? "Grant" : "Deny";
  const priority = permission_rule.priority ? "[P]" : undefined;
  return (<div><span>{priority}</span> <span>{grant}</span> <span>{permission_rule.permission}</span></div>);
}

export function PermissionRulesView({permission_rules}) {
  const header = <h4>Permission Rules</h4>
  const list = permission_rules.length > 0 ?
    <ul> {permission_rules.map(
      (permission_rule) => <li key={permission_rule.permission}>
        <SinglePermissionRuleView permission_rule={permission_rule}/></li>)}
    </ul> :
    <span>None</span>;
  const footer = permission_rules.length > 0 ?
    <span>[P] Priority Rules</span> :
    undefined;

  return (<div>{header}{list}{footer}</div>);
}

function createUpdateLink(role) {
  return role.updatable() ?
    <Link to={
      { pathname: `/roles/edit/${role.name}`,
        state: {
          values: {
            name: role.name,
            description: role.description,
            permission_rules: role.permission_rules,
          },
          readOnly: {
            name: true,
            description: !role.permissions.update_description,
            permission_rules: !role.permissions.update_permission_rules || !role.capabilities.update_permission_rules
          }
        }
      }}>Update</Link> : undefined;
}

export function SingleRoleView(props) {
  let params = useParams();
  const acm = new AccessControlManagementService();
  const roleView = (role) => {
    const update_link = createUpdateLink(role);
    const delete_link = role.deletable() ?
      <Link to={`/roles/delete/${role.name}`}>Delete</Link> :
      undefined;
    const know_users = role.users ?
      <div><h4>Known Users</h4>
        <ul> {
          role.users.map((user) => <li
            key={`${user.name}@${user.realm}`}>
            <Link to={
              `/users/${user.realm}/${user.name}`
            }>{`${user.name}@${user.realm}`}</Link>
          </li>)}
        </ul>
      </div> :
      undefined;
    const permission_rules = <PermissionRulesView permission_rules={role.permission_rules}/>;
    return (
      <div className="caosdb-single-user">
        <h1>{role.name}</h1>
        <div>Description: {role.description}</div>
        {permission_rules}
        {know_users}
        {update_link}
        {delete_link}
      </div>
    );
  }
  const onError = (error) => {
    console.log(error);
    return (
      <div>Error: {error.message}</div>
    );
  }
  return (
    <Await promise={acm.getRole(params.id)} then={roleView} catch={onError} />
  );
}
