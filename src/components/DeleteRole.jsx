import React, { useState } from 'react';
import { useHistory, useParams } from "react-router-dom";
import { AccessControlManagementService } from "../AccessControlManagementService";


export function DeleteRole() {
  let params = useParams();
  const [error, setError] = useState(null);
  const history = useHistory();

  const acm = new AccessControlManagementService();
  const delete_callback = async () => {
    try {
      await acm.deleteRole(params.id);
      history.push("/users");
    } catch (error) {
      setError(error.message);
    }
  }

  return (<div><h1>Delete Role {params.id}</h1>
    <p>Do you really want to delete this role?</p>
    <button onClick={delete_callback}>Delete {params.id}</button>
    {error ? <div>{error}</div> : undefined}
    </div>);
}
