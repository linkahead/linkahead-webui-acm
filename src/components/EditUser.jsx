import { AccessControlManagementService } from "../AccessControlManagementService";
import { UserForm } from "./CreateUser";
import { useLocation } from "react-router";

export function EditUser() {
  var location = useLocation();

  const acm = new AccessControlManagementService();

  return (<div>
    <h1>Edit User</h1>
    <UserForm submitLabel={"Save"}
              rolesList={acm.listRoles()}
              acmService={acm.updateUser}
              readOnly={location.state.readOnly}
              values={location.state.values}/>
    </div>);
}
