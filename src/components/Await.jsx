import React from 'react';
import { LoadingInfo } from "@indiscale/caosdb-webui-core-components";

export class Await extends React.Component {
  constructor(props) {
    super(props);
    const loading = ("loading" in props ? props.loading : <LoadingInfo/>);
    this.state = {
        result: loading,
    };

    const setResult = (result) => {
      this.setState({result: result});
    }

    var promise = props.promise;
    if(props.then) {
      promise = promise.then(props.then);
    }
    if(props.catch) {
      promise = promise.catch(props.catch);
    }
    promise.then(setResult);
  }
  render() {
    return (
      <React.Fragment>
        {this.state.result}
      </React.Fragment>
    );
  }
}
