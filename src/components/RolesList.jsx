import React from 'react';
import { Link } from "react-router-dom";
import { Await } from "./Await";
import { LoadingInfo } from "@indiscale/caosdb-webui-core-components";
import { AccessControlManagementService } from "../AccessControlManagementService";


function RolesListItem({role}) {
  return (
    <tr>
      <td>
        <Link to={`${role.name}`}>{role.name}</Link>
      </td>
      <td>
        {role.description}
      </td>
    </tr>
  );
}

export class RolesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: <LoadingInfo/>,
      error: null,
    };

  }
  render() {
    const acm = new AccessControlManagementService();
    const toRolesListItems = (roles_list) => {
        this.setState({...this.state, loading: undefined});
        return roles_list.map((role) => <RolesListItem key={role.name} role={role}/>
    )}

    const onError = ({message, stack, code, metadata}) => {
      // implicitely remove this.state.loading
      this.setState({error: message});
    }
    return (
      <div className="caosdb-role-list-component">
        <h1>Roles List</h1>
        {this.state.error ? this.state.error :
          <React.Fragment>
            <table>
              <thead>
                <tr>
                  <th>Name</th><th>Description</th>
                </tr>
              </thead>
              <tbody>
                <Await promise={acm.listRoles()} then={toRolesListItems} catch={onError} loading={false} />
              </tbody>
            </table>
            {this.state.loading}
          </React.Fragment>
        }
      </div>
    );
  }
}
