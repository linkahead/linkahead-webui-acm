import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import { ServerVersionInfo, UserComponent } from '@indiscale/caosdb-webui-core-components';
import { SingleEntityAcl, ListKnownPermissions, DeleteRole, EditRole, DeleteUser, EditUser, SingleRoleView, RolesList, SingleUser, UserList, CreateUser, CreateRole } from "./components";
import './index.css';

class UserManagementOverview extends React.Component {
  render() {
    return (
      <div className="caosdb-user-management-overview">
        <h1>User Management Overview</h1>
      </div>
    );
  }
}

function App() {
  return (
    <div className="caosdb-user-management">
      <Router basename="/ext">
        <Link to="/users/">list users</Link><span> - </span>
        <Link to="/users/new">new user</Link><span> - </span>
        <Link to="/roles/">list roles</Link><span> - </span>
        <Link to="/roles/new">new role</Link>
        <UserComponent />
        <Switch>
          <Route path="/" exact>
            <UserManagementOverview />
          </Route>
          <Route path="/users/" exact>
            <UserList/>
          </Route>
          <Route path="/users/new" exact>
            <CreateUser/>
          </Route>
          <Route path="/users/edit/:realm/:id/">
            <EditUser />
          </Route>
          <Route path="/users/delete/:realm/:name/">
            <DeleteUser />
          </Route>
          <Route path="/users/:realm/:id/">
            <SingleUser />
          </Route>
          <Route path="/roles/" exact>
            <RolesList/>
          </Route>
          <Route path="/roles/new" exact>
            <CreateRole/>
          </Route>
          <Route path="/roles/edit/:id/">
            <EditRole/>
          </Route>
          <Route path="/roles/delete/:id/">
            <DeleteRole/>
          </Route>
          <Route path="/roles/:id/">
            <SingleRoleView/>
          </Route>
          <Route path="/permissions/">
            <ListKnownPermissions/>
          </Route>
          <Route path="/entityacl/:id">
            <SingleEntityAcl/>
          </Route>
        </Switch>
        <ServerVersionInfo/>
      </Router>
    </div>
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
