# CaosDB Webui 2 - Module for Access Control Management

## Setup Development Environment


* Install all deps `npm install`
* Start node development server `npm start`
* Start proxy (for grpc-web/http1 <-> grpc/http2) `docker run -p 8081:8081 -e ENVOY_UID=$(id -u) -v $(pwd)/envoy.yaml:/etc/envoy/envoy.yaml --network host envoyproxy/envoy:v1.20.0`
* Start a caosdb-server in branch f-grpc-f-acm which listens on 10443 (or configure envoy.yaml)
* Browse to localhost:8081/ext/ (localhost:8081 has the normal webinterface)


## Generate grpc java script library

Download/Install Protoc
Download/Install GRPC-Web plugin https://github.com/grpc/grpc-web/releases/tag/1.3.0


E.g.:

    PROTO_MODULE=entity # [entity,info,acm]
    PROTOC=$HOME/.conan/data/protobuf/3.17.1/_/_/package/e8224316157fcb4b4f98b4ef6205feedbbc2add3/bin/protoc-3.17.1.0
    PROTO_DIR=$HOME/src/caosdb-server/caosdb-proto/proto
    PROTO_FILE=${PROTO_DIR}/caosdb/$PROTO_MODULE/v1alpha1/main.proto
    GRPCWEB_PLUGIN=$HOME/tmp/protoc-gen-grpc-web

    OUT_DIR=$HOME/src/caosdb-webui2/generated
    ${PROTOC} --plugin=${GRPCWEB_PLUGIN} -I=${PROTO_DIR} ${PROTO_FILE} \
        --js_out=import_style=commonjs:$OUT_DIR \
        --grpc-web_out=import_style=commonjs,mode=grpcwebtext:$OUT_DIR
